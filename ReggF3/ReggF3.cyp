// cypher-import für ReggFIII
// Lemmaknoten in Actionknoten umbenannt
// importiert alle Verben
// die korrigierten Ortsdaten eingebunden
// Alle Ausstellungsorte der RI (Pfad: DA/neo4j/Regesta-Imperii/Ortsdaten/)
// Wikidata-Verwandtschaften werden auch importiert
// Im Register IS-CHILD_OF durch IS_SUB_OF ersetzt
//https://seafile.rlp.net/f/98ca512c1f36457faf1a/?dl=1 

// Die Links beziehen sich auf die Google-Docs im Ordner Daten
// F3-alle-Verben
// https://docs.google.com/spreadsheets/d/1EW97klqyL7BhhhRZWK6ospZFVrkrUVoEweTF9JhsZSQ/export?format=csv&id=1EW97klqyL7BhhhRZWK6ospZFVrkrUVoEweTF9JhsZSQ&gid=857114297 
// Alle Ausstellungsorte
// https://docs.google.com/spreadsheets/d/1Y_HoBKkRkwh4LxIFFzQJHZkB0W57dWoGfdeq00Fi6cg/export?format=csv&id=1Y_HoBKkRkwh4LxIFFzQJHZkB0W57dWoGfdeq00Fi6cg&gid=1929188235 
// erstmal alles löschen
CALL apoc.periodic.iterate('MATCH (n) RETURN n', 'DETACH DELETE n', {batchSize:1000});
CALL apoc.schema.assert({},{},true) YIELD label, key
RETURN *;

CREATE INDEX IF NOT EXISTS FOR (n:IndexPerson) ON (n.registerid);
CREATE INDEX IF NOT EXISTS FOR (n:Date) ON (n.startDate);
CREATE INDEX IF NOT EXISTS FOR (n:Place) ON (n.original);
CREATE INDEX IF NOT EXISTS FOR (n:Place) ON (n.normalizedGerman);
CREATE INDEX IF NOT EXISTS FOR (n:Lemma) ON (n.lemma);
CREATE INDEX IF NOT EXISTS FOR (n:Literature) ON (n.literatur);
CREATE INDEX IF NOT EXISTS FOR (n:Reference) ON (n.reference);
CREATE INDEX IF NOT EXISTS FOR (n:IndexEntry) ON (n.registerId);
CREATE INDEX IF NOT EXISTS FOR (n:IndexEntry) ON (n.xmlId);
CREATE INDEX IF NOT EXISTS FOR (n:IndexEntry) ON (n.nodeId);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.latLong);
CREATE INDEX IF NOT EXISTS FOR (n:IndexPlace) ON (n.registerId);
CREATE INDEX IF NOT EXISTS FOR (n:IndexEvent) ON (n.registerId);
CREATE INDEX IF NOT EXISTS FOR (n:IndexPerson) ON (n.registerId);
CREATE INDEX IF NOT EXISTS FOR (n:IndexPerson) ON (n.wikidataId);
CREATE INDEX IF NOT EXISTS FOR (n:ExternalResource) ON (n.url);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.regid);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.regestaNumber);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.regestaVolume);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.origPlaceOfIssue);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.startDate);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.endDate);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.title);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.incipit);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.originalDate);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.externalLinks);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.exchangeIdentifier);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.urn);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.pid);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.uid);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.sorting);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.bandpk);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.laufendenummer);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.regestennummernorm);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.identifier);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.heftId);
CREATE INDEX IF NOT EXISTS FOR (n:Regesta) ON (n.date);

CALL apoc.periodic.iterate('LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/regesta-imperii-data/-/raw/main/data/regesta-csv/RI_alles.csv" AS line FIELDTERMINATOR "\t"
WITH line
WHERE line.identifier STARTS WITH "[RI XIII]"
WITH line
RETURN line',
'CREATE (r:Regesta {regid:line.persistent_identifier, origPlaceOfIssue:line.`locality_string`, startDate:line.`start_date`, endDate:line.`end_date`, summary:line.`summary`, archivalHistory:line.`archival_history`, title:line.`title`, commentary:line.`commentary`, literature:line.`literature`, footnotes:line.`footnotes`, annotations:line.`annotations`, incipit:line.`incipit`, originalDate:line.`original_date`, versoNote:line.`verso_note`, seal:line.`seal`, recipient:line.`recipient`, witnesses:line.`witnesses`, clerk:line.`clerk`, chancellor:line.`chancellor`, signature:line.`signature`, signatureAddition:line.`signature_addition`, externalLinks:line.`external_links`, exchangeIdentifier:line.`exchange_identifier`, urn:line.`urn`, pid:line.`pid`, uid:line.`uid`, sorting:line.`sorting`, bandpk:line.`bandpk`, laufendenummer:line.`laufendenummer`, regestennummernorm:line.`regestennummernorm`, identifier:line.`identifier`, date:line.`date_string`});', {batchSize:1000});

// External Links erstellen
MATCH (reg:Regesta)
WHERE reg.externalLinks CONTAINS "link"
UNWIND apoc.text.regexGroups(reg.externalLinks,
"<link (\\S*?)( - \\S*?)>(.*?)</link>") as link
MERGE (er:ExternalResource {url:link[1]})
ON CREATE SET er.title=link[3]
MERGE (reg)-[:EXTERNAL_SOURCE]->(er);

// RI-Ausstellungsorte-geo erstellen
CALL apoc.periodic.iterate('LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/1Y_HoBKkRkwh4LxIFFzQJHZkB0W57dWoGfdeq00Fi6cg/export?format=csv&id=1Y_HoBKkRkwh4LxIFFzQJHZkB0W57dWoGfdeq00Fi6cg&gid=1929188235"
AS line
WITH line
WHERE line.normalisiertDeutsch IS NOT NULL
RETURN line',
'MATCH (r:Regesta {origPlaceOfIssue:line.Original})
MERGE (p:Place {normalizedGerman:line.normalisiertDeutsch})
WITH r, p, line
SET p.longitude = line.Long
SET p.latitude = line.Lat
MERGE (r)-[:PLACE_OF_ISSUE]->(p);', {batchSize:1000});


// PLACE_OF_ISSUE-Kanten mit zusätzlichen Informationen versehen
CALL apoc.periodic.iterate('LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/1Y_HoBKkRkwh4LxIFFzQJHZkB0W57dWoGfdeq00Fi6cg/export?format=csv&id=1Y_HoBKkRkwh4LxIFFzQJHZkB0W57dWoGfdeq00Fi6cg&gid=1929188235"
AS line
WITH line
RETURN line',
'MATCH (p:Place {normalizedGerman:line.normalisiertDeutsch})<-[rel:PLACE_OF_ISSUE]-(reg:Regesta {origPlaceOfIssue:line.Original})
SET rel.original = line.Original
SET rel.alternativeName = line.Alternativname
SET rel.commentary = line.Kommentar
SET rel.allocation = line.Zuordnung
SET rel.state = line.Lage
SET rel.certainty = line.Sicherheit
SET rel.institutionInCity = line.InstInDerStadt
SET p.longitude = line.Long
SET p.latitude = line.Lat;', {batchSize:1000});

// Regesten und Ausstellungsorte mit neo4j-Koordinaten der Ausstellungsorte versehen
MATCH (o:Place)
WHERE o.latitude IS NOT NULL
SET o.latLong = o.latitude + ',' + o.longitude ;
MATCH (r:Regesta)-[:PLACE_OF_ISSUE]->(o:Place)
SET r.nLatLong = point({latitude: tofloat(o.latitude), longitude: tofloat(o.longitude)})
SET o.nLatLong = point({latitude: tofloat(o.latitude), longitude: tofloat(o.longitude)})
SET r.placeOfIssue = o.normalizedGerman
SET r.latitude = o.latitude
SET r.longitude = o.longitude
SET r.latLong = o.latLong;

LOAD CSV WITH HEADERS FROM 'https://docs.google.com/spreadsheets/d/1EW97klqyL7BhhhRZWK6ospZFVrkrUVoEweTF9JhsZSQ/export?format=csv&id=1EW97klqyL7BhhhRZWK6ospZFVrkrUVoEweTF9JhsZSQ&gid=857114297'
AS line FIELDTERMINATOR ','
MATCH (r:Regesta{identifier:line.regId})
WITH line, r, apoc.text.replace(line.verben, '\'', '') AS v1
WITH line, r, apoc.text.replace(v1, '\\{', '') AS v2
WITH line, r, apoc.text.replace(v2, '\\}', '') AS v3
FOREACH ( j in split(v3, ', ') |
MERGE (l:Action {action:j})
MERGE (l)<-[:ACTION]-(r));

// <unknown>-Action löschen>
MATCH (l:Action {action:'<unknown>'})
DETACH DELETE l;

// <set>-Action löschen>
MATCH (l:Action {action:'set()'})
DETACH DELETE l;

// ReggFIII-Literaturnetzwerk mit Seitenzahlen erstellen
MATCH (r:Regesta)
WHERE r.archivalHistory CONTAINS "link"
UNWIND apoc.text.regexGroups(r.archivalHistory, "link (\\S+)>(\\S+)</link>,? ([\\w\\s]+? S. [\\d\\-f\\.]+?)[;<]+") as link
MERGE (ref:Literature {url:link[1]}) ON CREATE SET ref.title=toUpper(link[2])
MERGE (bel:Reference {referenceID:link[3]}) ON CREATE SET bel.title=link[2]+' '+link[3] SET bel.reference=link[3]
MERGE (r)-[:CITED_PAGE]->(bel)
MERGE (bel)-[:LITERATURE]->(ref);

// ReggFIII-Quellenverweise erstellen
MATCH (r:Regesta)
WHERE r.archivalHistory CONTAINS "link"
UNWIND apoc.text.regexGroups(r.archivalHistory, "link (\\S+)>(\\S+)</link>,? ([\\w\\s]+? n. [\\d\\-f\\.]+?)[;<]+") as link
MERGE (ref:Literature {url:link[1]}) ON CREATE SET ref.title=toUpper(link[2])
MERGE (bel:Reference {referenceID:link[3]}) ON CREATE SET bel.title=link[2]+' '+link[3] SET bel.reference=link[3]
MERGE (r)-[:CITED_RECORD]->(bel)
MERGE (bel)-[:LITERATURE]->(ref);

// RegID und HeftId für F3-Hefte als Property erstellen
MATCH (reg:Regesta)
WHERE NOT reg.identifier CONTAINS 'Supplement'
UNWIND apoc.text.regexGroups(reg.identifier, ".* (\\S+) n. (\\S+).*") as link
SET reg.registerId = ("#" + link[1] + "-" + link[2])
SET reg.heftId = link[1]
SET reg.shortNumber = ("#" + link[1] + "-" + link[2]);

// URLs für Regesten erstellen
MATCH (reg:Regesta)
WHERE reg.regid IS NOT NULL
SET reg.url = ("http://www.regesta-imperii.de/id/" + reg.regid);
//MATCH (reg:Regesta)
//WHERE reg.persistentIdentifier IS NOT NULL
//SET reg.url = ("http://www.regesta-imperii.de/id/" + reg.persistentIdentifier);

// Falsche Datumsangaben finden
MATCH (reg:Regesta)
UNWIND apoc.text.regexGroups(reg.startDate, "^(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)$") as date WITH date, reg
WHERE toInteger(date[2]) > 12
OR toInteger(date[2]) = 0
OR toInteger(date[3]) > 31
OR toInteger(date[3]) = 0
SET reg.startDateWrong = reg.startDate
SET reg.startDate = '2021-01-01';
MATCH (reg:Regesta)
UNWIND apoc.text.regexGroups(reg.endDate, "^(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)$") as date WITH date, reg
WHERE toInteger(date[2]) > 12
OR toInteger(date[2]) = 0
OR toInteger(date[3]) > 31
OR toInteger(date[3]) = 0
SET reg.endDateWrong = reg.endDate
SET reg.endDate = '2021-01-01';

// Regesten mit falschen startDate und endDate-Einträgen löschen
MATCH (n:Regesta)
WHERE n.startDate =~ '.*-00-00'
DETACH DELETE n;

// Datum als Neo4j-Datum ergänzen
MATCH (n:Regesta)
SET n.isoStartDate = date(n.startDate);
MATCH (n:Regesta)
SET n.isoEndDate = date(n.endDate);

// Date in Isodatum umwandeln
MATCH (n:Regesta)
SET n.isoStartDate = date(n.startDate);
MATCH (n:Regesta)
SET n.isoEndDate = date(n.endDate);
MATCH (d:Date)
SET d.isoStartDate = date(d.startDate);
MATCH (d:Date)
SET d.isoEndDate = date(d.endDate);


// IndexEntries erstellen

CALL apoc.load.xml('https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/regesta-imperii-data/-/raw/main/data/indexes/RI_013.xml?inline=false','',{}, true) yield value as xmlFile
//CALL apoc.load.xml('https://exist.regesta-imperii.de/exist/rest/apps/rixqlib/xql/getRegister.xql?registerName=013','',{}, true) yield value as xmlFile
UNWIND xmlFile._register AS wdata
CREATE (e:IndexEntry {xmlId:wdata.id, type:wdata.typ, parentId:wdata.parent, latitude:wdata.lat, longitude:wdata.lon, wikidataId:wdata.wikidata, geonames:wdata.geonames})
RETURN count(e);

// label bei den IndexEntries ergänzen
CALL apoc.load.xml('https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/regesta-imperii-data/-/raw/main/data/indexes/RI_013.xml','',{}, true) yield value as xmlFile
//CALL apoc.load.xml('https://exist.regesta-imperii.de/exist/rest/apps/rixqlib/xql/getRegister.xql?registerName=013','',{}, true) yield value as xmlFile
UNWIND xmlFile._register AS lemma
WITH lemma.id AS xmlId,
[x in lemma._lemma where x._type="label" | x._text][0] AS label,
[y in [x in lemma._lemma where x._type ="numbers" | x._numbers][0] where y.type="nennung" | y._text] AS nennung,
[y in [x in lemma._lemma where x._type ="numbers" | x._numbers][0] where y.type="empfaenger" | y._text] AS empfaenger
MATCH (i:IndexEntry {xmlId:xmlId})
SET i.label = label;

// wikidataId ergänzen
CALL
apoc.load.xml('https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/regesta-imperii-data/-/raw/main/data/indexes/RI_013.xml','',{}, true) yield value as xmlFile
//CALL apoc.load.xml('https://exist.regesta-imperii.de/exist/rest/apps/rixqlib/xql/getRegister.xql?registerName=013','',{}, true) yield value as xmlFile
UNWIND xmlFile._register AS lemma
WITH lemma.id AS xmlId,
[x in lemma._lemma WHERE x._type='idno' AND x.type='wikidata'| x._text][0] AS wikidataId
WITH wikidataId, xmlId WHERE wikidataId IS NOT NULL
MATCH (i:IndexEntry {xmlId:xmlId})
SET i.wikidataId = wikidataId
RETURN count(i);

//IS_SUB_OF erstellen
MATCH (c:IndexEntry), (p:IndexEntry)
WHERE c.parentId = p.xmlId
MERGE (c)-[r:IS_SUB_OF]->(p)
RETURN count(r);


// APPEARS_IN erstellen
CALL
apoc.load.xml('https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/regesta-imperii-data/-/raw/main/data/indexes/RI_013.xml?inline=false','',{}, true) yield value as xmlFile
//CALL apoc.load.xml("https://exist.regesta-imperii.de/exist/rest/apps/rixqlib/xql/getRegister.xql?registerName=013",'',{}, true) yield value as xmlFile
UNWIND xmlFile._register AS wdata
UNWIND wdata._lemma AS lemma
UNWIND lemma._numbers AS number
MATCH (e:IndexEntry {xmlId:wdata.id})
MATCH (r:Regesta  {registerId:number._text})
WITH wdata,e,r,number
WHERE number.type = "nennung"
MERGE (e)-[ai:APPEARS_IN]->(r)
RETURN count(ai);

// RECIPIENT IN erstellen
CALL
apoc.load.xml('https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/regesta-imperii-data/-/raw/main/data/indexes/RI_013.xml?inline=false','',{}, true) yield value as xmlFile
//CALL apoc.load.xml("https://exist.regesta-imperii.de/exist/rest/apps/rixqlib/xql/getRegister.xql?registerName=013",'',{}, true) yield value as xmlFile
UNWIND xmlFile._register AS wdata
UNWIND wdata._lemma AS lemma
UNWIND lemma._numbers AS number
MATCH (e:IndexEntry {xmlId:wdata.id})
MATCH (r:Regesta  {registerId:number._text})
WITH wdata,e,r,number
WHERE number.type = "empfaenger"
MERGE (e)-[ri:RECIPIENT_IN]->(r)
RETURN count(ri);

// PLACE_OF_ISSUE erstellen
CALL
apoc.load.xml('https://gitlab.rlp.net/adwmainz/regesta-imperii/lab/regesta-imperii-data/-/raw/main/data/indexes/RI_013.xml?inline=false','',{}, true) yield value as xmlFile
//CALL apoc.load.xml("https://exist.regesta-imperii.de/exist/rest/apps/rixqlib/xql/getRegister.xql?registerName=013",'',{}, true) yield value as xmlFile
UNWIND xmlFile._register AS wdata
UNWIND wdata._lemma AS lemma
UNWIND lemma._numbers AS number
MATCH (e:IndexEntry {xmlId:wdata.id})
MATCH (r:Regesta  {registerId:number._text})
WITH wdata,e,r,number
WHERE number.type = "austOrt"
MERGE (e)-[ri:PLACE_OF_ISSUE {source:'index'}]->(r)
RETURN count(ri);

// Registerstufen anlegen
MATCH (n1:IndexEntry)
SET n1.pathLength = 'zero';
MATCH (n1:IndexEntry)
WHERE NOT (n1)-[:IS_SUB_OF]->(:IndexEntry)
SET n1.pathLength = '0';
MATCH p=(n1:IndexEntry{pathLength:'0'})<-[r:IS_SUB_OF*..8]-(n2)
WHERE n2.pathLength = 'zero'
SET n2.pathLength = toString(length(p));

// Indexeinträge mit neo4j-Koordinaten der Ausstellungsorte versehen
MATCH (e:IndexEntry)
WHERE e.latitude IS NOT NULL
SET e.nlatLong = point({latitude: tofloat(e.latitude), longitude: tofloat(e.longitude)})
;

// Indexeinträge mit kommagetrennten Koordinaten der Ausstellungsorte versehen
MATCH (e:IndexEntry)
WHERE e.latitude IS NOT NULL
SET e.latLong = e.latitude + ',' + e.longitude ;

// IndexEntry mit weiteren Labels ausstatten
MATCH (e:IndexEntry)
WHERE e.type = 'person'
WITH e
CALL apoc.create.addLabels(id(e), ['IndexPerson']) YIELD node
RETURN node;

MATCH (e:IndexEntry)
WHERE e.type = 'ereignis'
WITH e
CALL apoc.create.addLabels(id(e), ['IndexEvent']) YIELD node
RETURN node;

MATCH (e:IndexEntry)
WHERE e.type = 'sache'
WITH e
CALL apoc.create.addLabels(id(e), ['IndexThing']) YIELD node
RETURN node;

MATCH (e:IndexEntry)
WHERE e.type = 'ort'
WITH e
CALL apoc.create.addLabels(id(e), ['IndexPlace']) YIELD node
RETURN node;

// Registereintragspfade erstellen
Match (e0:IndexEntry)<-[:IS_SUB_OF]-(e1:IndexEntry)
SET e1.path = e0.label + ' // ' + e1.label;
Match (e0:IndexEntry {pathLength:'0'})<-[:IS_SUB_OF]-(e1:IndexEntry)<-[:IS_SUB_OF]-(e2:IndexEntry)
SET e2.path = e0.label + ' // ' + e1.label + ' // ' + e2.label;
Match (e0:IndexEntry {pathLength:'0'})<-[:IS_SUB_OF]-(e1:IndexEntry)<-[:IS_SUB_OF]-(e2:IndexEntry)<-[:IS_SUB_OF]-(e3:IndexEntry)
SET e3.path = e0.label + ' // ' + e1.label + ' // ' + e2.label + ' // ' + e3.label;
Match (e0:IndexEntry {pathLength:'0'})<-[:IS_SUB_OF]-(e1:IndexEntry)<-[:IS_SUB_OF]-(e2:IndexEntry)<-[:IS_SUB_OF]-(e3:IndexEntry)<-[:IS_SUB_OF]-(e4:IndexEntry)
SET e4.path = e0.label + ' // ' + e1.label + ' // ' + e2.label + ' // ' + e3.label + ' // ' + e4.label;
Match (e0:IndexEntry {pathLength:'0'})<-[:IS_SUB_OF]-(e1:IndexEntry)<-[:IS_SUB_OF]-(e2:IndexEntry)<-[:IS_SUB_OF]-(e3:IndexEntry)<-[:IS_SUB_OF]-(e4:IndexEntry)<-[:IS_SUB_OF]-(e5:IndexEntry)
SET e5.path = e0.label + ' // ' + e1.label + ' // ' + e2.label + ' // ' + e3.label + ' // ' + e4.label + ' // ' + e5.label;
Match (e0:IndexEntry {pathLength:'0'})<-[:IS_SUB_OF]-(e1:IndexEntry)<-[:IS_SUB_OF]-(e2:IndexEntry)<-[:IS_SUB_OF]-(e3:IndexEntry)<-[:IS_SUB_OF]-(e4:IndexEntry)<-[:IS_SUB_OF]-(e5:IndexEntry)<-[:IS_SUB_OF]-(e6:IndexEntry)
SET e6.path = e0.label + ' // ' + e1.label + ' // ' + e2.label + ' // ' + e3.label + ' // ' + e4.label + ' // ' + e5.label + ' // ' + e6.label;

// Überlieferungssituation klären
MATCH (reg:Regesta)
WHERE reg.archivalHistory CONTAINS "link"
UNWIND apoc.text.regexGroups(reg.archivalHistory, "<link reg:(\\S*?)>(.*?)</link>") as link
MATCH (r2:Regesta {regid:link[1]})
CREATE (reg)-[:RELATED_TO]->(r2);
 
MATCH (n:Regesta)
SET n.laufendenummer = toInteger(n.laufendenummer);
 
LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/e/2PACX-1vSn-a4ay2qvrPhv2p9AFKuuO0aaffE0CyUTvJH-yP_Pa3jCDLVYdgrYpL1eFiI4GBXTQ2qBTbAsqhY1/pub?gid=1529598223&single=true&output=csv"
AS line
WITH line
WHERE line.volume IS NOT NULL
MATCH (r:Regesta {heftId:line.volume})
SET r.yearOfPublication = line.year
SET r.region = line.region;
 
LOAD CSV WITH HEADERS FROM "https://docs.google.com/spreadsheets/d/e/2PACX-1vSn-a4ay2qvrPhv2p9AFKuuO0aaffE0CyUTvJH-yP_Pa3jCDLVYdgrYpL1eFiI4GBXTQ2qBTbAsqhY1/pub?gid=1529598223&single=true&output=csv"
AS line
WITH line
WHERE line.volume IS NOT NULL
MATCH (n:Regesta)
WHERE n.heftId = line.volume
WITH n ORDER BY n.laufendenummer
// WITH n ORDER BY n.heftId, n.laufendenummer ASC
WITH collect(n) as reg
CALL apoc.nodes.link(reg, "NEXT_REGESTA")
return count(reg);

