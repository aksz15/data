<!DOCTYPE HTML>
<html lang="de">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Die Urkunden Heinrichs&nbsp;V.: 236. Utrecht, 1122 Mai 26</title>
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
      <link rel="manifest" href="/site.webmanifest">
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="/mstile-144x144.png">
      <meta name="theme-color" content="#ffffff">
      <link rel="stylesheet" href="MGHStyleDDHV.css" title="MGH Diplomata Style" type="text/css">
   </head>
   <body>
      <header>
         <div id="headerMGH">
            <div id="mghLogo"><a href="http://www.mgh.de"></a></div>
            <div id="headerMiddle"> </div>
            <div id="mghKranz"><a href="http://www.mgh.de"></a></div>
         </div>
         <nav>
            <div id="startseite" class="navbar_entry"><a href="index.htm">Startseite</a></div>
            <div id="inhalt" class="navbar_entry"><a href="toc.htm">Urkunden</a></div>
            <div id="konkordanz" class="navbar_entry"><a href="stumpf.htm">Konkordanztabelle</a></div>
            <div id="zeichen" class="navbar_entry"><a href="abb.htm">Zeichen und Abkürzungen</a></div>
         </nav>
         <nav class="breadcrumbs"><img src="https://data.mgh.de/assets/images/pointing_hand.svg" width="23" height="26" alt="U+261E: WHITE RIGHT POINTING INDEX"> &nbsp;<a href="http://www.mgh.de">MGH</a> &nbsp;→&nbsp; <a href="https://www.mgh.de/mgh-digital/digitale-angebote-zu-mgh-abteilungen">Datenbanken</a></nav>
         <div id="headerTitle">Die Urkunden Heinrichs V. und der Königin Mathilde</div>
      </header>
      <div id="mainWrapper">
         <main>
            
            <div id="c_236" class="charter">
               
               <div class="kopfregest">
                  
                  <div id="kopfregestHead"><span><a href="dhv_235.htm">&lt;&lt;</a></span><span><span class="bold">236.</span></span><span><a href="dhv_237.htm">&gt;&gt;</a></span></div>
                  
                  <p><span class="italics">Heinrich bestätigt den Kirchen St. <span class="spaced">Martin</span>
                        und St. <span class="spaced">Marien</span>
                        zu <span class="spaced">Utrecht</span>
                        die königlichen Schenkungen im Gau Ijssel und Lek, restituiert den
                        Pröpsten die durch den Grafen Wilhelm und dessen Vorgänger usurpierten
                        Gerichtsrechte und bestätigt ihnen die dem Grafen Wilhelm wegen seiner
                        Rebellion gegen den Kaiser im Fürstengericht aberkannte Grafschaft.</span></p>
                  
                  <p class="ortdatum right"><span class="italics">Utrecht, 1122 Mai 26.</span></p>
                  </div>
               
               
               <div lang="la" class="text">
                  
                  <p>In nomine sanctę
                     et individuę
                     trinitatis. Heinricus
                     divina
                     favente clementia
                     [quartus]
                     Romanorum imperator augustus. Quę
                     a predecessoribus
                     nostris pię
                     memorię imperatoribus sive
                     regibus sanctis ęcclesiis
                     dei
                     collata sunt et postea inminuta
                     sive neglecta comperimus, decet regalem iusticiam ea supplere et
                     restaurare ob spem  ęternę
                     remunerationis, et hoc acceptabile credimus coram
                     deo et hominibus. Quia igitur ex antiquis primum ęcclesiarum munimentis, deinde
                     renovantibus et confirmantibus hoc ipsum paginis et sigillis suis pię memorię
                     imperatore augusto Heinrico
                     secundo
                     et
                     avo nostro Heinrico
                     tercio, totum, quod in pago 
                     est Isla et Lake, cognovimus regali munificentia traditum 
                     duobus 
                     monasteriis
                     in Traiecto, uni
                     in honore sancti Martini, alteri
                     in
                     honore
                     sanctę dei genitricis Marię omniumque sanctorum dedicato, placuit nobis eandem piorum
                     regum traditionem regali
                     nostra
                     auctoritate
                     confirmare, hoc addentes, ut, quod a Wilhelmo
                     comite
                     eiusque predecessoribus aliquibus
                     violenter
                     usurpatum est, amodo supradictis ęcclesiis
                     stabili et perpetua regalitatis
                     nostrę
                     traditione inconvulsum permaneat, scilicet ut, sicut terra et
                     homines et
                     census
                     ad predictas ęcclesias attinent, ita omnis iusticia ad prepositos
                     earundem
                     ęcclesiarum et villicos eorum pertineat, et nullus ibi comes vel
                     advocatus preter
                     prepositos sit, sed omnis iusticia ad illos pertineat, sive in furtis sive in
                     aggeribus sive in bellicis navibus, que
                     vulgo silinc vocantur, vel quibuscumque aliis negociis
                     ad seculare placitum pertinentibus. Notum quoque sit omnibus regni
                     fidelibus, quod idem Wilhelmus
                     comes in presentia nostra in ipsa civitate Traiecti
                     contra nos bello et armis manum levavit, et ob hoc iudicio principum
                     et liberorum hominum eundem comitatum prepositis predictarum
                     ęcclesiarum favente et assistente eis iure ex antiquis regum
                     traditionibus inconvulsa auctoritate confirmavimus. Ut
                     autem
                     hęc traditio nostrę
                     auctoritatis
                     rata
                     permaneat, hanc paginam inde conscriptam manu propria roborantes
                     sigillo
                     nostro
                     insigniri iussimus.</p>
                  
                  <p>Signum domni
                     Heinrici
                     quarti
                     imperatoris invictissimi.</p>
                  
                  <p>Bruno cancellarius vice Adelberti archicancellarii recognovi.</p>
                  
                  <p> Data
                     VII. kl.
                     iunii, anno dominice
                     incarnationis
                     MCXXII, indictione
                     XV, anno autem domni
                     Heinrici
                     quinti regni
                     quidem
                     eius
                     XXIII, imperii vero
                     XII; actum est Traiecti; in dei
                     nomine
                     feliciter amen.</p>
                  </div>
               
               
               </div>
            
            </main>
      </div>
      <footer>
         <div id="footerContainer">
            <div id="footerLeft"><img id="footerMGHLogo" alt="MGH-Logo" src="https://data.mgh.de/assets/images/logo/svg/mgh_logo_white.svg"><a href="https://twitter.com/MghBibliothek"><img alt="Twitter" src="https://data.mgh.de/assets/images/twitter.svg"></a><a href="https://www.mgh.de/die-mgh/mgh-podcast"><img alt="Podcast" src="https://data.mgh.de/assets/images/podcast2.svg"></a></div>
            <div id="footerMiddle">
               <div id="footerMiddleContent">
                  <figure role="Verwaltung">
                     <address>
                        <p class="bold">Verwaltung</p>
                        <p>Tel. <a href="tel:+4989286382385">+49 89 28638-2385</a></p>
                        <p>Fax +49 89 28638-2180</p><a href="mailto:sekretariat@mgh.de">sekretariat@mgh.de</a></address>
                  </figure>
                  <figure role="Hausanschrift">
                     <address>
                        <p class="bold">Hausanschrift</p>
                        <p>Ludwigstraße 16</p>
                        <p>80539 München</p>
                     </address>
                  </figure>
                  <figure role="Postanschrift">
                     <address>
                        <p class="bold">Postanschrift</p>
                        <p>Postfach 34 02 23</p>
                        <p>80099 München</p>
                     </address>
                  </figure>
               </div>
            </div>
            <div id="footerRight">
               <section>
                  <figure role="Hilfreiches">
                     <h3>Hilfreich</h3><a href="https://www.mgh.de">MGH-Homepage</a><a href="https://www.mgh.de/die-mgh/kontakt">Kontakt</a></figure>
                  <figure role="Rechtliches">
                     <h3>Rechtliches</h3><a href="https://www.mgh.de/datenschutz/impressum">Impressum</a><a href="https://www.mgh.de/datenschutz">Datenschutz</a></figure>
               </section>
            </div>
         </div>
      </footer>
   </body>
</html>