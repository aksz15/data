#!/bin/bash

# Define the URL to the page with links to the charters
TOC_URL="https://data.mgh.de/databases/ddhv/toc.htm"

# Use curl to download the page with the list of charter links
curl $TOC_URL -o toc.html

# Extract the charter links. This command would vary depending on the page structure.
grep -oP 'href="\K[^"]*' toc.html > links.txt

# Define the base URL for relative links
BASE_URL="https://data.mgh.de/databases/ddhv/"

# Create a CSV file and add the header
echo "Metadata,Charter Text" > charters.csv

# Loop through the links and process each charter
while read -r link; do
  # Download the charter page
  curl "${BASE_URL}${link}" -o charter.html

  # Extract the relevant content and metadata using xmllint or pup
  # This is a placeholder; the actual command will depend on the HTML structure
  # and would need to be adjusted to your specific case.
  METADATA=$(xmllint --html --xpath '//div[@class="kopfregest"]/text()' charter.html 2>/dev/null)
  CHARTER_TEXT=$(xmllint --html --xpath '//div[@class="text"]/text()' charter.html 2>/dev/null)

  # Append the extracted content to the CSV file
  # Here we're using echo with double quotes to handle any commas or newlines in the text
  echo "\"$METADATA\",\"$CHARTER_TEXT\"" >> charters.csv

  # Provide a message that the charter has been processed
  echo "Processed charter: $link"
done < links.txt

echo "Finished downloading and processing charters. Data saved to charters.csv."

