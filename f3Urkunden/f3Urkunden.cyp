// f3UrkCypher
 
// erstmal alles löschen
CALL apoc.periodic.iterate('MATCH (n) RETURN n', 'DETACH DELETE n', {batchSize:1000});
CALL apoc.schema.assert({},{},true) YIELD label, key
RETURN *;
 
CREATE INDEX IF NOT EXISTS FOR (n:Archiv) ON (n.label);
CREATE INDEX IF NOT EXISTS FOR (n:Ort) ON (n.label);
 
// Import der Archiv-Hierarchie
CALL apoc.load.xml("https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/archiv-thesaurus.xml") YIELD value
UNWIND value._children AS child
WITH child
UNWIND child._children AS element
FOREACH (ignoreMe IN CASE WHEN element._type CONTAINS "prefTerm" THEN [1] ELSE [] END |
  CREATE (au:Archiv {label: element._text})
  FOREACH (elementAgain IN child._children |
    FOREACH (ignoreMe IN CASE WHEN elementAgain._type CONTAINS "creation" THEN [1] ELSE [] END |
      SET au.hierarchy = elementAgain._text)));
 
// Verknüpfung der Archivknoten
CALL apoc.load.xml("https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/archiv-thesaurus.xml") YIELD value
UNWIND value._children AS child
WITH child
UNWIND child._children AS element
FOREACH (ignoreMe IN CASE WHEN element._type CONTAINS "creation" THEN [1] ELSE [] END |
  FOREACH (elementAgain IN child._children |
    FOREACH (ignoreMe IN CASE WHEN elementAgain._type CONTAINS "broader" THEN [1] ELSE [] END |
      MERGE (au:Archiv {hierarchy:element._text})
      MERGE (au1:Archiv {hierarchy:elementAgain._text})
      MERGE (au)-[r:IS_SUB_OF]->(au1))));
 
// Import der Regionen-Hierarchie
CALL apoc.load.xml("https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/regional-thesaurus.xml") YIELD value
UNWIND value._children AS child
WITH child
UNWIND child._children AS element
FOREACH (ignoreMe IN CASE WHEN element._type CONTAINS "prefTerm" THEN [1] ELSE [] END |
  CREATE (au:Region {label: element._text})
  FOREACH (elementAgain IN child._children |
    FOREACH (ignoreMe IN CASE WHEN elementAgain._type CONTAINS "creation" THEN [1] ELSE [] END |
      SET au.hierarchy = elementAgain._text)));
 
// Verknüpfung der Regionsknoten
CALL apoc.load.xml("https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/regional-thesaurus.xml") YIELD value
UNWIND value._children AS child
WITH child
UNWIND child._children AS element
FOREACH (ignoreMe IN CASE WHEN element._type CONTAINS "creation" THEN [1] ELSE [] END |
  FOREACH (elementAgain IN child._children |
    FOREACH (ignoreMe IN CASE WHEN elementAgain._type CONTAINS "broader" THEN [1] ELSE [] END |
      MERGE (au:Region {hierarchy:element._text})
      MERGE (au1:Region {hierarchy:elementAgain._text})
      MERGE (au)-[r:IS_SUB_OF]->(au1))));
 
// Import der Sachbetreff-Hierarchie
CALL apoc.load.xml("https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/regional-thesaurus.xml") YIELD value
UNWIND value._children AS child
WITH child
UNWIND child._children AS element
FOREACH (ignoreMe IN CASE WHEN element._type CONTAINS "prefTerm" THEN [1] ELSE [] END |
  CREATE (au:Sachbetreff {label: element._text})
  FOREACH (elementAgain IN child._children |
    FOREACH (ignoreMe IN CASE WHEN elementAgain._type CONTAINS "creation" THEN [1] ELSE [] END |
      SET au.hierarchy = elementAgain._text)));
 
// Verknüpfung der Sachbetreffknoten
CALL apoc.load.xml("https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/regional-thesaurus.xml") YIELD value
UNWIND value._children AS child
WITH child
UNWIND child._children AS element
FOREACH (ignoreMe IN CASE WHEN element._type CONTAINS "creation" THEN [1] ELSE [] END |
  FOREACH (elementAgain IN child._children |
    FOREACH (ignoreMe IN CASE WHEN elementAgain._type CONTAINS "broader" THEN [1] ELSE [] END |
      MERGE (au:Sachbetreff {hierarchy:element._text})
      MERGE (au1:Sachbetreff {hierarchy:elementAgain._text})
      MERGE (au)-[r:IS_SUB_OF]->(au1))));
 
 
 
 
CALL apoc.periodic.iterate('LOAD CSV WITH HEADERS FROM "https://git.thm.de/aksz15/data/-/raw/master/f3Urkunden/f3Urkunden.csv" AS line FIELDTERMINATOR ","
WITH line RETURN line', '
CREATE (r:Urkunde {Objektnummer:line.Objektnummer, Objektart:line.Objektart, Amtsbezeichnung:line.Amtsbezeichnung, Anbringung:line.Anbringung, Angaben_zum_Original:line.Angaben_zum_Original, Anmerkungen_zur_Kopie:line.Anmerkungen_zur_Kopie, Archiv:line.Archiv, Art_der_Kopie:line.Art_der_Kopie, Ausstellungsort:line.Ausstellungsort, Bemerkungen:line.Bemerkungen, Beschreibstoff:line.Beschreibstoff, Besiegelung_Anmerkungen:line.Besiegelung_Anmerkungen, Besiegelung_der_Kopie:line.Besiegelung_der_Kopie, Biographisches:line.Biographisches, Chmelbeleg:line.Chmelbeleg, ChmelNr:line.ChmelNr, ChmelRegg:line.ChmelRegg, ChmelText:line.ChmelText, Datierungszeile_der_Kopie:line.Datierungszeile_der_Kopie, Datum_der_Kopie:line.Datum_der_Kopie, Diplomatische_Form:line.Diplomatische_Form, Druckbeleg:line.Druckbeleg, EditionsDatum:line.EditionsDatum, Empfänger:line.Empfänger, Erfasser:line.Erfasser, Ergibt_sich_aus:line.Ergibt_sich_aus, Erwähnung:line.Erwähnung, Familienname:line.Familienname, Foto:line.Foto, IDNummer:line.IDNummer, IndexDatum:line.IndexDatum, Kanzlist:line.Kanzlist, Kopie:line.Kopie, KopieStoff:line.KopieStoff, Korrektur:line.Korrektur, Korrigiert_von:line.Korrigiert_von, Kurzregest:line.Kurzregest, KV_Anmerkungen:line.KV_Anmerkungen, KVr:line.KVr, KVrecto:line.KVrecto, KVRPosition:line.KVRPosition, KVv:line.KVv, KVVPosition:line.KVVPosition, Lebensdaten:line.Lebensdaten, Literatur:line.Literatur, Maske:line.Maske, Mehrfachüberlieferung_von:line.Mehrfachüberlieferung_von, Nachtragsfelder:line.Nachtragsfelder, Orte:line.Orte, Ortsangaben:line.Ortsangaben, Ortsname:line.Ortsname, Ortsname_der_Quelle:line.Ortsname_der_Quelle, Personen:line.Personen, Personenname:line.Personenname, Pön:line.Pön, Quellendatum:line.Quellendatum, Referent:line.Referent, Regestbeleg:line.Regestbeleg, ReggF:line.ReggF, ReggFApparat:line.ReggFApparat, ReggFKV:line.ReggFKV, ReggFText:line.ReggFText, ReggFÜberlieferung:line.ReggFÜberlieferung, Region:line.Region, RegisterText:line.RegisterText, Registrator:line.Registrator, Registraturvermerk:line.Registraturvermerk, RF:line.RF, RFIII:line.RFIII, RfNr:line.RfNr, RR:line.RR, Sachbetreff:line.Sachbetreff, Schnurfarbe:line.Schnurfarbe, Sekretsiegel_PosseNr:line.Sekretsiegel_PosseNr, Sekretsiegelfarbe:line.Sekretsiegelfarbe, Sekretsiegellage:line.Sekretsiegellage, SF:line.SF, Siegel_PosseNr:line.Siegel_PosseNr, Siegelfarbe:line.Siegelfarbe, Signatur:line.Signatur, Sonstiges:line.Sonstiges, SortierDatum:line.SortierDatum, Taxreg:line.Taxreg, UNummer:line.UNummer, UrkNummer:line.UrkNummer, Urkunde:line.Urkunde, UrkundenNummer:line.UrkundenNummer, Wasserzeichen:line.Wasserzeichen, Weitere_Nachweise:line.Weitere_Nachweise, Weitere_Überlieferung:line.Weitere_Überlieferung, Zugang:line.Zugang})', {batchSize:1000});
 
 
 
// Archiv erstellen
match (u:Urkunde)
where u.Archiv is not null
merge (a:Archiv {label:u.Archiv})
merge (a)<-[:ARCHIV]-(u);
 
// Region verlinken
CREATE INDEX IF NOT EXISTS FOR (n:Region) ON (n.label);
match (u:Urkunde)
where u.Region is not null
merge (a:Region {label:u.Region})
merge (a)<-[:REGION]-(u);
 
// Sachbetreff 
CREATE INDEX IF NOT EXISTS FOR (n:Sachbetreff) ON (n.label);
match (u:Urkunde)
where u.Sachbetreff is not null
merge (a:Sachbetreff {label:u.Sachbetreff})
merge (a)<-[:SACHBETREFF]-(u);
 
// Ausstellungsort erstellen
match (u:Urkunde)
where u.Ausstellungsort is not null
merge (a:Ort {label:u.Ausstellungsort})
merge (a)<-[:AUSSTELLUNGSORT]-(u);
 
// Empfänger
CREATE INDEX IF NOT EXISTS FOR (n:Empfaenger) ON (n.label);
match (u:Urkunde)
where u.`Empfänger` is not null
merge (a:Empfaenger {label:u.`Empfänger`})
merge (a)<-[:EMPFAENGER]-(u);
 
// Dipl Form
CREATE INDEX IF NOT EXISTS FOR (n:DiplomatischeForm) ON (n.label);
match (u:Urkunde)
where u.Diplomatische_Form is not null
merge (a:DiplomatischeForm {label:u.Diplomatische_Form})
merge (a)<-[:DIPLOMATISCHE_FORM]-(u);
 
// Beleg
CREATE INDEX IF NOT EXISTS FOR (n:Beleg) ON (n.label);
match (u:Urkunde)
where u.Literatur is not null
FOREACH ( j in split(u.Literatur, ";") |
MERGE (t:Beleg {label:trim(j)})
MERGE (t)<-[:BELEG]-(u)
);
 
// Literatur
CREATE INDEX IF NOT EXISTS FOR (n:Literatur) ON (n.label);
MATCH (n:Beleg)
WHERE n.label CONTAINS "/"
UNWIND apoc.text.regexGroups(n.label, "^(.*?) / (.*?)$") as link
MERGE (l:Literatur {label:link[1]})
MERGE (n)-[:LITERATUR]->(l);
 
// Druckbeleg
CREATE INDEX IF NOT EXISTS FOR (n:Druckbeleg) ON (n.label);
match (u:Urkunde)
where u.Druckbeleg is not null
FOREACH ( j in split(u.Druckbeleg, ";") |
MERGE (t:Beleg {label:trim(j)})
MERGE (t)<-[:DRUCKBELEG]-(u)
);

