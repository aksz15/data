#!/usr/bin/php -q
<?

include_once("inc_xmlparser.php");
include_once("inc_myarray.php");

$parser=new CXMLParser;


$archivkonvert=
'Göttingen-DA	Göttingen-Diplomatischer Apparat
Wien-HHStA	Wien-HHStaatsarchiv
DiplApp	Diplomatischer Apparat
StadtA	Stadtarchiv
SolmsA	Solmsisches Archiv
DiözA	Diözesanarchiv
NatIns	Nationalinstitut
StadtB	Stadtbibliothek
HarsdorfA Harsdorfer Archiv
BistumA	Bistumsarchiv
HallerA	Hallerarchive
StiftA	Stiftsarchiv
HohenloheZA	Hohenlohisches Zentralarchiv
Bfl. ZA	Bischöfliches Zentralarchiv
BibMarciana	Biblioteca Marciana
DiplApp	Diplomatischer Apparat
PuPBib	Bibliothek
UnivA	Universitätsarchiv
HStA	Hauptstaatsarchiv
ÖNat	Österreichische Nationalbibliothek
EKHN	Evangelische Kirche von Hessen und Nassau
FÖSA	Fürstlich oettingisch-spiegelbergisches Archiv
FÖWA	Fürstlich oettingisch-wallersteinisches Archiv
GNMB	Bibliothek des Germanischen Nationalmuseums
GNMA	Archiv des Germanischen Nationalmuseums
TTZA	Thurn-und-Taxis-Zentralarchiv
VHVO	Historischer Verein der Oberpfalz
Bibl	Bibliothek
FWA	Fürstlich Wiedisches Archiv
LKA	Landeskirchenarchiv
BSB	Bayerische Staatsbibliothek
GLA	Generallandesarchiv
LHA	Landeshauptarchiv
GStA	Geheimes Staatsarchiv
StA	Staatsarchiv
PfA	Pfarrarchiv
Bib	Bibliothek
SBB	Staatsbibliothek
DSA	Domstiftsarchiv
Dep	Archive departementale
AV	Stadtarchiv
ULB	Landesbibliothek
DO	Deutschordensarchiv
SB	Stadtbibliothek
LB	Landesbibliothek
DA	Domarchiv
HA	Hausarchiv
RA	Reichsarchiv
UB	Universitätsbibliothek
LA	Landesarchiv
FA	Fürstliches Archiv
A	archiv
B	bibliothek';

foreach(explode("\n",$archivkonvert) as $aline){
	$archivtokens=explode("\t",$aline);
	global $archiv_from_strings;
	global $archiv_to_strings;
	$archiv_from_strings[]='/'.strval(trim($archivtokens[0]).'$').'/';
	$archiv_to_strings[]=strval(trim($archivtokens[1]));
}

function archivkonvertierung($aname){
	$res="";
	foreach (explode("<br>",$aname) as $archivname) {
			$archivname=trim($archivname);
			if ($archivname) {
			global $archiv_from_strings;
			global $archiv_to_strings;
			$res.= preg_replace($archiv_from_strings,$archiv_to_strings,$archivname);
			$res.="<br>";
		}
	}
	return $res;
}


function urkundentyp($text) {
	if ($text=='Original') {
		return 1;
	} else
	if ($text=='Kopial') {
		return 2;
	} else
	if ($text=='Deperditum') {
		return 3;
	} 
	if ($text=='Literaturbeleg') {
		return 4;
	} else
	if ($text=='Reichsregister') {
		return 5;
	} else
	return 0;
}

	


function error($msg){
	fprintf (STDERR, $msg);
	fprintf (STDERR, "\n");
	exit;
}

function field_write($text){
	$text=str_replace("\r","",$text);
	$text=str_replace("\n","<lb>",$text);
	$text=str_replace("\t","&#9;",$text);
	echo $text;
}



class myCallbackClass extends CXMLCallback {
	
	var $printCreate=0;
	
	var $typ=0;
	var $fields;
	var $fieldname="";
	var $dataread="";
	var $lnr=0;
	
	
	function myCallbackClass(){
		$this->reset();
	}

	function reset(){
		$this->printCreate=0;
		$this->lnr=0;
		$this->nextItem();
	}
	
	function nextItem(){
		$this->fields=new myField;
		$this->fields->addfield('Urkunden-Nummer');
		$this->fields->addfield('UrkNummer');

		$this->fields->addfield('Index-Datum');
		$this->fields->addfield('Quellendatum');
		$this->fields->addfield('Ausstellungsort');
		$this->fields->addfield('Kurzregest');

		$this->fields->addfield('Rf-Nr');
		$this->fields->addfield('RF-III');
		$this->fields->addfield('Chmelbeleg');
		$this->fields->addfield('ChmelRegg');
		$this->fields->addfield('ReggF');

		$this->fields->addfield('RR');
		$this->fields->addfield('Taxreg');
		$this->fields->addfield('Druckbeleg');
		$this->fields->addfield('Regestbeleg');
		$this->fields->addfield('Literatur');
		$this->fields->addfield('Erwähnung');


		$this->fields->addfield('Region');
		$this->fields->addfield('Orte');
		$this->fields->addfield('Empfänger');
		$this->fields->addfield('Personen');
		$this->fields->addfield('Sachbetreff');
		$this->fields->addfield('Foto');
		$this->fields->addfield('Pön');
		$this->fields->addfield('Wasserzeichen');

//?????
		$this->fields->addfield('Weitere_Nachweise');
		$this->fields->addfield('Mehrfachüberlieferng_von');
		$this->fields->addfield('Weitere_Überlieferung');
		$this->fields->addfield('Nachtragsfelder');
		$this->fields->addfield('Bemerkungen');
		$this->fields->addfield('Sonstiges');
		$this->fields->addfield('Ergibt_sich_aus');


//Archivnachweise
		$this->fields->addfield('Archiv');
		$this->fields->addfield('Signatur');



//Kanzleivermerke
		$this->fields->addfield('KVr');
		$this->fields->addfield('KVRPosition');
		$this->fields->addfield('KVv');
		$this->fields->addfield('KVVPosition');
		$this->fields->addfield('KV-recto');
		$this->fields->addfield('KV_-_Anmerkungen');
		$this->fields->addfield('Kanzlist');
		$this->fields->addfield('Amtsbezeichnung');
		$this->fields->addfield('Referent');
		$this->fields->addfield('Registraturvermerk');
		$this->fields->addfield('Registrator');

//Formalangaben
		$this->fields->addfield('Beschreibstoff');
		$this->fields->addfield('Diplomatische_Form');

//Besiegelung
		$this->fields->addfield('Anbringung');
		$this->fields->addfield('Schnurfarbe');
		$this->fields->addfield('Siegel_-Posse-Nr_-');
		$this->fields->addfield('Siegelfarbe');
		$this->fields->addfield('Sekretsiegel_-Posse-Nr_-');
		$this->fields->addfield('Sekretsiegelfarbe');
		$this->fields->addfield('Sekretsiegellage');
		$this->fields->addfield('Besiegelung_-_Anmerkungen');


//Deperditum:
		$this->fields->addfield('Art_der_Kopie');
		$this->fields->addfield('Datum_der_Kopie');
		$this->fields->addfield('Datierungszeile_der_Kopie');
		$this->fields->addfield('Besiegelung_der_Kopie');
		$this->fields->addfield('Anmerkungen_zur_Kopie');
		$this->fields->addfield('Angaben_zum_Original');


		$this->dataread="";
		$this->typ="";
		$this->fieldname="";
		$this->lnr++;
	}
	
	function tagOpen(&$text) {
		$ut=urkundentyp($text);
		if ($ut) {
			if ($this->typ) {
				error("E: Letzte Urkunde nicht geschlossen");
				exit;
			} else {
				$this->typ=$ut;
				//echo $ut;
			}
		} else {
			if ($this->fields->exists($text)) {
				$this->fieldname=$text;
			}
		}
	}
	
	function tagClose(&$text) {
		$ut=urkundentyp($text);
		if ($ut) {
			if ($this->typ!=$ut) {
				error("E: Unerwarteter Urkundenabschluss");
				exit;
			} else {
				//echo "\n\n".$this->fields->get("Urkunden-Nummer").":\n";	
				//output($this->dat);
				$this->typ=0;
				$d=$this->fields->get("Index-Datum");
				preg_match("/([0-9]+)? ?([0-9]+)? ?([0-9]+)?.?/",$d,$treffer);
					//echo "Datum: $d\n";
					//print_r($treffer);
				
				$this->fields->addfield("jahr");
				$this->fields->set("jahr",intval($treffer[1]));
				$this->fields->addfield("monat");
				$this->fields->set("monat",intval($treffer[2]));
				$this->fields->addfield("tag");
				$this->fields->set("tag",intval($treffer[3]));
				$this->fields->addfield("typ");
				$this->fields->set("typ",$ut);
				$this->fields->addfield("lnr");
				$this->fields->set("lnr",$this->lnr);
				
				$this->fields->toInt("Urkunden-Nummer");
				$this->fields->toInt("UrkNummer");
				$this->fields->toInt("jahr");
				$this->fields->toInt("monat");
				$this->fields->toInt("tag");
				$archivname=$this->fields->get("Archiv");
				$this->fields->set("Archiv",archivkonvertierung($archivname));
				if (!$this->printCreate) {
					echo $this->fields->format_CREATE_TABLE("rfurk");
					$this->printCreate=1;
				}
				 
				echo $this->fields->format_INSERT("rfurk");
				$this->nextItem();
			}
		} else {
			if ($text==$this->fieldname) {
				$tmp=$this->fields->get($this->fieldname);
				if (($tmp!="") && ($this->fieldname!="UrkNummer")){
					$tmp.="<br>".$this->dataread;
				} else {
				    $tmp=$this->dataread;
				}
				$tmp=str_replace(array("\r\n","\n","\r"),"<br>",$tmp);
				$this->fields->set($this->fieldname,$tmp);
				$this->fieldname="";
				$this->dataread="";
			}
		}
	}
	
	function content(&$text) {
		if ($this->fieldname!="") {
			$this->dataread.=$text;
		}
	}

	function notify(&$itemtype,&$text) {
		switch ($itemtype) {
			case XML_TOKEN_TAG_OPEN:
				$this->tagOpen($text);
				break;
			case XML_TOKEN_TAG_CLOSE:
				$this->tagClose($text);
				break;
			case XML_TOKEN_TEXT:
				$this->content($text);
				break;
		}
		//echo "myCallback itemtype: $itemtype\n";
		//echo "myCallback text: $text\n";
	}
}

$callback1=new myCallbackClass;
$parser->callbackObj=$callback1;


//echo "argv=";
//var_dump($argv);

$filename_in=$argv[1];

if ( ($filename_in=="") ) {
	error( "Start: RIurkconv.php <INPUTFILE>");
	exit;
}


$filein=0;
$fileout=0;


//echo "Opening input file $filename_in...\n";



if ($filein=fopen($filename_in, "rb")) {
//	echo "OK.\n";
//	echo "Converting Data...\n";
	while ($x=fgets($filein)) {
		$parser->parseString($x);
		$parser->parseByte(chr(10));
		//fwrite($fileout, $x);
	}
} else {
	error ("~fopen failed on input file~");
	exit;
}


fclose($filein);



?>
