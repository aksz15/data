<?

include_once("inc_xmlparser.php");

$parser=new CXMLParser;

//echo "argv=";
//var_dump($argv);

$filename_in=$argv[1];
$filename_out=$argv[2];

if ( ($filename_in=="") or ($filename_out=="") ) {
	echo "Start: example_xml.php <INPUTFILE> <OUTPUTFILE>";
}


$filein=0;
$fileout=0;

echo "Opening output file $filename_out...\n";

if ($fileout=fopen($filename_out, "wb")) {
	echo "Ok.\n";	
} else {
	echo "~fopen failed on output file~";
	exit;
}


echo "Opening input file $filename_in...\n";


if ($filein=fopen($filename_in, "rb")) {
	echo "OK.\n";
	echo "Converting Data...\n";
	while ($x=fgets($filein)) {
		$i=0;
		$l=strlen($x);
		while ($i<$l) {
			//echo "[".$x[$i]."]";
			$parser->parseByte($x[$i]);
			if ($parser->itemread){
				echo "Item: ".$parser->itemtext."\n";
			}
			$i++;
		}
		//fwrite($fileout, $x);
	}
} else {
	echo "~fopen failed on input file~";
	fclose($fileout);
	exit;
}


fclose($filein);
fclose($fileout);



?>