<?
class CTextFile {
	
	var $isOpen;
	var $filename;
	var $fin;

	function EOF(){
		return feof($this->fin);
	}

	function readByte(){
		return fread($this->fin, 1);
	
	}
	
	function close(){
	    return fclose($this->fin);
	
	}
	
	
	function open($filename){
		$this->filename="";
        	if (file_exists($filename)) {
			$this->fin = fopen($filename,"rb");
			if ($this->fin) {
	        		$this->filename=$filename;
			}
			return ($this->fin<>FALSE) ;
		} else return FALSE;
        }
        

        
}


?>


