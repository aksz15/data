<?

function array_key_of_value($ar,$value) {
	if (!is_array($ar)) {
		echo "\nWARNUNG array_key_of_value ~noarray\n";
		return "";
	}
	foreach ($ar as $k => $v){
		if ($v==$value) return $k;
	}
	return "";
}

function array_contains($ar,$value) {
	if (!is_array($ar)) {
		echo "\nWARNUNG array_key_of_value ~noarray\n";
		return 0;
	}
	foreach ($ar as $v){
		if ($v==$value) return 1;
	}
	return 0;
}

function unicode2dbname($ustr) {
	$ustr=str_replace("ä","ae",$ustr);
	$ustr=str_replace("Ä","Ae",$ustr);
	$ustr=str_replace("ü","ue",$ustr);
	$ustr=str_replace("Ü","Ue",$ustr);
	$ustr=str_replace("ö","oe",$ustr);
	$ustr=str_replace("Ö","oe",$ustr);
	$ustr=str_replace("ß","sz",$ustr);
	$ustr=str_replace("-","_",$ustr);
	$res="";
	for ($i=0;$i<strlen($ustr);$i++) {
		$o = ord($ustr{$i});
		if (! (
			(($o>=0x30) and ($o<=0x39)) or 
			(($o>=0x41) and ($o<=0x5A)) or 
			(($o>=0x61) and ($o<=0x7A)) 
			) ) {
			$ustr{$i} = "_";
		}
	}
	return $ustr;
}


class myArray {
	var $a=array();

	function clear(){
		$this->a=array();
	}

	function add($v){
		$a=$v;
	}

	function myArray(){
		$this->clear();
	}
	
}

class myField {
	private $fields=array();
	private $data=array();
	
	function myField() {
	$this->init();
	}
	
	function init(){
		$this->fields=array();
		$this->data=array();
	
	}
	
	function clear(){
		$this->data=array();	
	}

	function initNumeric($count){
		$this->init();
		for ($i=0; $i<$count; $i++) {
			addfield($i);
		}
	}

	function initDB($item){
		$this->init();
		foreach ($item as $k => $v){
			$this->addfield($k);
			$this->set($k,$v);
			}
	}

	function initSplit($splitchar,$s) {
		$this->init();
		$k=0;
		foreach (split($splitchar,$s) as $v){
			$this->addfield($k);
			$this->set($k,$v);
			$k++;
		}
	}

	function format_CSV($seperator){
	$res="";
	foreach ($this->fields as $fn) {
		$v=$this->get($fn);
		if (is_string($v)) {
			$res.=$cm.strval($v);
		} else {
			$res.=$cm.strval($v);
		}
		if ($cm=="") $cm=$seperator;
	}
	return $res."\n";
	}

	function format_TABV($tabreplace){
		$res="";
		foreach ($this->fields as $fn) {
			$v=strval($this->get($fn));
			$v=str_replace("\t",$tabreplace,$v);
			$res.=$cm.strval($v);
			if ($cm=="") $cm="\t";
		}
		return $res."\n";
	}

	function format_TABV_HEADER($tabreplace){
		$res="";
		foreach ($this->fields as $fn) {
			$v=str_replace("\t",$tabreplace,$fn);
			$res.=$cm.strval($v);
			if ($cm=="") $cm="\t";
		}
		return $res."\n";
	}


	function format_CREATE_TABLE($tablename){
		if ($tablename=="") $tablename='<TABLENAME>';
		$res='CREATE TABLE '.$tablename.' (';	
		$cm="";
		foreach ($this->fields as $fn) {
			$res.=$cm.unicode2dbname($fn);
			if ($this->isInt($fn)) {
				$res.=" int";
			} else {
				$res.=" text";
			}
			if ($cm=="") $cm=", ";
		}
		$res.= ");\n";
		return $res;
	}
	
	
	function format_INSERT($tablename){
		if ($tablename=="") $tablename='<TABLENAME>';
		$res='INSERT INTO '.$tablename.' (';	
		$cm="";
		foreach ($this->fields as $fn) {
			$res.=$cm.unicode2dbname($fn);
			if ($cm=="") $cm=", ";
		}
		$res.=") values (";
		$cm="";
		foreach ($this->fields as $fn) {
			$v=$this->get($fn);
			if (is_int($v)) {
				$res.=$cm.strval($v);
			} else {
				$res.=$cm."'".strval(pg_escape_string(strval($v)))."'";
			}
			if ($cm=="") $cm=", ";
		}
		$res.= ");\n";
		return $res;
	}

	function printIni() {
		foreach ($this->fields as $fname ){
			echo $fname;
			echo "=";
			echo $this->get($fname);
			echo "\n";
		}
	}
	
	function exists($fieldname) {
		$fieldname=strval($fieldname);
		if ($fieldname=="") return 0;
		return array_contains($this->fields,$fieldname);
	}
	
	function addfield($fieldname){
		$fieldname=strval($fieldname);
		if ($fieldname=="") return 0;
		if (!$this->exists($fieldname)) {
			$this->fields[]=$fieldname;
			return 1;
		}
		return 0;
	}
	
	function get($fieldname){
		$fieldname=strval($fieldname);
		return $this->data[$fieldname];
	}
	
	function set($fieldname, $value){
		$fieldname=strval($fieldname);
		if ($fieldname=="") return 0;
		if (!$this->exists($fieldname)) return 0;
		$this->data[$fieldname]=$value;
		return 1;
	}
	
	function toInt($fieldname){
		$fieldname=strval($fieldname);
		if ($fieldname=="") return 0;
		if (!$this->exists($fieldname)) return 0;
		$this->data[$fieldname]=intval($this->data[$fieldname]);
		return 1;
	}

	function toStr($fieldname){
		$fieldname=strval($fieldname);
		if ($fieldname=="") return 0;
		if (!$this->exists($fieldname)) return 0;
		$this->data[$fieldname]=strval($this->data[$fieldname]);
		return 1;
	}
	
	function isStr($fieldname){
		return is_string($this->data[$fieldname]);
	}
	function isInt($fieldname){
		return is_int($this->data[$fieldname]);
	}
}

?>