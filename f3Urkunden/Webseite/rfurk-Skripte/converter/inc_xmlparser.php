<?



define("XML_TOKEN_ERROR",0);
define("XML_TOKEN_START",1);
define("XML_TOKEN_TAG",2);

//Returned by $this->itemtype :
define("XML_TOKEN_TEXT",3);
define("XML_TOKEN_TAG_OPEN",4);
define("XML_TOKEN_TAG_CLOSE",5);
define("XML_TOKEN_WHITESPACE",6);
define("XML_TOKEN_PI",7);
define("XML_TOKEN_CDATA",8);
define("XML_TOKEN_COMMENT",9);
define("XML_TOKEN_TAG_EMPTY",10);

class CXMLCallback{
	function notify(&$itemtype,&$text) {
		echo "Callback itemtype: $itemtype\n";
		echo "Callback text: $text\n";
	}
}


class CXMLParser {
	var $state;
	var $opentags;
	var $text;
	
	var $syntaxerror;
	var $errormessage;

	var $ypos;
	var $xpos;
	
	var $scanAsEntity;
	var $textEntity;

	var $itemtype;
	
	var $callbackObj;
	
	function reset(){
		$this->state=XML_TOKEN_START;
		$this->opentags=array();
		$this->text="";
		$this->syntaxerror=0;
		$this->scanAsEntity=0;
		$this->textEntity="";
		$this->itemread=0;
		$this->itemtype=0;
		$this->itemtext='';
		$this->xpos=0;
		$this->ypos=0;
		$this->callbackObj=0;
	}
	
	function errorSignal($msg){
		$this->syntaxerror=true;
		$this->state=XML_TOKEN_ERROR;
		echo $this->text;
		echo "{}";
		$this->errormessage="\n\nERROR:\nLine: ".($this->y+1)." Pos: ".($this->x+1)." // ".$msg;
		echo $this->errormessage;
		exit;
		
	}

	function CXMLParser() {
		$this->reset();
	}
	
	
	function notify($typenr){
		$this->callbackObj->notify($typenr,$this->text);
		$this->itemtype=0;
		$this->text='';
	}
	
	function isWhitespace(&$b){
		switch ($b) {
			case "\t" :
			case " "  :
			case "\n" :
			case "\r" :
				return 1;
			default:
				return 0;
		}
	}
	
	function isLinebreak(&$b){
		return ($b=="\n");
	}
	
	function updatePos(&$b){
		if ($this->isLinebreak($b)) {
			$this->y++;
			$this->x=0;
		}
		else {
			$this->x++;
		}
	}
	

	
	function scanPlaintext(&$b) {
		switch ($b) {
			case "<" : 
				$this->notify(XML_TOKEN_TEXT);
				$this->state=XML_TOKEN_TAG;
				break;
			case ">" : 
				$this->errorSignal("Char {>} not allowed in text");
				break;
			case "'" : 
				$this->errorSignal("Char {'} not allowed in text");
				break;
			case "\"" : 
				$this->errorSignal("Char {\"} not allowed in text");
				break;
			default:
				$this->text.=$b;
				break;
		}
	}
	
	function scanWhitespace(&$b) {
		if ($this->isWhitespace($b)) {
				$this->text.=$b;
		} else
		switch ($b) {
			case "<" : 
				$this->notify(XML_TOKEN_WHITESPACE);
				$this->state=XML_TOKEN_TAG;
				break;
			case ">" : 
				$this->errorSignal("Char {>} not allowed in text");
				break;
			case "'" : 
				$this->errorSignal("Char {'} not allowed in text");
				break;
			case "\"" : 
				$this->errorSignal("Char {\"} not allowed in text");
				break;
			default:
				$this->text.=$b;
				$this->state=XML_TOKEN_TEXT;
				break;
		}
	}


	
	function scanTagOpen(&$b) {
		switch ($b) {
			case ">":
				$this->notify(XML_TOKEN_TAG_OPEN);
				$this->state=XML_TOKEN_START;
				break;
			case "<":
				$this->errorSignal("Char {<} not allowed in tag");
				break;
			default:
				$this->text.=$b;
		}
	}
	
	function scanTagClose(&$b) {
		switch ($b) {
			case ">":
				$this->notify(XML_TOKEN_TAG_CLOSE);
				$this->state=XML_TOKEN_START;
				break;
			case "<":
				$this->errorSignal("Char {<} not allowed in tag");
				break;
			default:
				$this->text.=$b;
		}
	}
	
	function scanTag(&$b) {
		switch ($b) {
			case "/":
				$this->state=XML_TOKEN_TAG_CLOSE;
				break;
			default:
				$this->state=XML_TOKEN_TAG_OPEN;
				$this->text.=$b;
		}
	}
	
	function parseByte($b) {
		//if ($this->syntaxerror) return 0;
		
		switch ($this->state) {
			case XML_TOKEN_START:
				if ($b=='<') {
					$this->state=XML_TOKEN_TAG;
				} else if ($this->isWhitespace($b)) {
					$this->state=XML_TOKEN_WHITESPACE;
					$this->scanWhitespace($b);
				} else {
					$this->state=XML_TOKEN_TEXT;
					$this->scanPlaintext($b);
				}
				break;
			case XML_TOKEN_TEXT:
				$this->scanPlaintext($b);
				break;
			case XML_TOKEN_TAG_OPEN:
				$this->scanTagOpen($b);
				break;	
			case XML_TOKEN_TAG_CLOSE:
				$this->scanTagClose($b);
				break;	
			case XML_TOKEN_TAG:
				$this->scanTag($b);
				break;	
			case XML_TOKEN_WHITESPACE:
				$this->scanWhitespace($b);
				break;
			default:
				$this->errorSignal("parseByte: Unknown State in Switch");
				exit;
				
		}
		$this->updatePos($b);
		if ($this->syntaxerror) return 0;
		return 1;
	}
	
	
	function parseString(&$s){
		$l=strlen($s);
		for ($i=0; $i<$l; $i++) {
			$this->parseByte($s[$i]);
		}
	}
	
}


?>