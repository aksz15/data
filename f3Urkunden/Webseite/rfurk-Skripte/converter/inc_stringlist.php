<?

class CStringList {
	
	var $lines;
	var $count;
	var $breakAtEndOfFile;
	var $tag;
	
	function reset(){
		$this->lines=array();
		$this->breakAtEndOfFile=false;
		$this->count=0;
		$this->tag="";
	}
	
	function CStringList(){
		$this->reset();
	}
	
	function insert($linenr, $anystring) {
			while ($this->count<$linenr) $this->add("");				
			$array2 = array_splice($this->lines,$linenr);
			$this->lines[]=$anystring;
			$this->lines = array_merge($this->lines,$array2);
			$this->count++;	
	}
	
	function delete($linenr){
		if ($linenr<$this->count) {
			unset($this->lines[$linenr]);
			$this->count--;
			echo "del.";
			return true;
		} else return false;
	}
	
	function add($s) {
		$this->lines[]=$s;
		$this->count++;
	}
	
	function assign($anystring){
		$this->reset();
		$l=strlen($anystring);
		$t="";
		$endofline=false;
		for ($i=0; $i<$l; $i++) {
			switch ($anystring[$i]) {
				case "\n": case "\r" :
					$endofline=true;
					break;
				default:
					if ($endofline) {
						$this->add($t);
						$t="";
					} 
					$t=$t.$anystring[$i];
					$endofline=false;
					break;
			}	
		}
		if ($t<>"") $this->add($t);
		$breakAtEndOfFile=$endofline;
	}
}


?>