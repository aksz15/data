<?

/* 

Hier: OHNE FLAG-Tabelle, OHNE FLAG-FOREIGN-KEY-CONSTRAINTS!

*/


include_once("inc_unicodec.php");

function wordlist_count($db,$wpk,$flag){
	if (!$wpk) {
		echo "ERROR Kein wpk in wordlist_count\n";
		exit;
	}
	$res=pg_exec($db,"select * from wordcount where wpk=$wpk and flagbit=$flag");
	if ( $r=pg_fetch_array($res) ){
		$new=$r["counter"]+1;
		pg_Exec($db,"update wordcount set counter=".$new. " where wpk=$wpk and flagbit=$flag");
	} else {
		pg_exec($db,"insert into wordcount (wpk, flagbit, counter) values ($wpk, $flag, 1)");
	}
}

function wordlist_addwords($db,$pk,$text,$flag) {
	//preg_match_all("/\b\d+\b/",$text,$matches);
	preg_match_all("/[\pL\d]+/u",$text,$matches);
	//print_r($matches); exit;
	for ($i=0; $i<count($matches[0]); $i++) {
		//echo $matches[0][$i]."\n";
		$w=$matches[0][$i];
		if (($w)=="") continue;
		$wnorm=utf8_normalize($w);
		//echo "Word='$w'\n";
		$res=pg_exec($db,"select * from wordlist where word = '".$w."' limit 1");
		$nextpos=0;
		$nextpos2=0;
		$l1=utf8_getchar($wnorm,0,$nextpos);
		if ($l1===false) continue;
		$l2=utf8_getchar($wnorm,$nextpos,$nextpos2);
		if ($l2===false) $l2=32;
		$l1=unicode_norm_register($l1);
		$l2=unicode_norm_register($l2);
		$wnormlower=strtolower($wnorm);
		$wpk=-1;
		$newflags=$flag;
		if ($r=pg_fetch_array($res)) {
			//echo "Wort vorhanden: $w\n";
			$wpk=$r["pk"];
			$newflags=$r["flags"] | $flag;
			pg_exec($db,"update wordlist set flags=$newflags where pk=$wpk");
		} else {
			//echo "insert into wordlist (word, wordnorm, l1, l2, flags) values ('".$w."','".$wnorm."',$l1,$l2,$flag);\n";
			pg_exec($db,"insert into wordlist (word, wordnorm, wordnormlower, l1, l2, flags) values ('".$w."','".$wnorm."','".$wnormlower."',$l1,$l2,$flag);");
			$wpkres=pg_exec("select currval('wordlist_pk_seq'::regclass)");
			$wpkr=pg_fetch_array($wpkres);
			$wpk=$wpkr[0];
		}
		if ($wpk!=-1) {
			wordlist_count($db,$wpk,$flag);
		}
		$reslink=pg_exec($db,"select * from wordlink where wpk=$wpk and pk=$pk");
		if ($rlink=pg_fetch_array($reslink)) {
			$newflags=$rlink["flags"] | $flag;
			pg_exec($db,"update wordlink set flags=$newflags where wpk=$wpk and pk=$pk");
		} else {
			pg_exec($db,"insert into wordlink (wpk,pk,flags) values ($wpk, $pk,$flag)");
		}
	}
}

function wordlist_setflags($db,$flags){
	$bit=1;
	for ($i=0; $i<count($flags); $i++){
		$flagx=$flags[$i];
		$sql= "insert into wordflags (flagbit,flagname,colname,registerdepth) values (".$bit.",'".$flagx[0]."','".$flagx[1]."','".$flagx[2]."')";
		echo $sql;
		pg_exec($db,$sql);
		$bit=$bit<<1;
	}
}

function twopower($hochzahl) {
	if ($hochzahl<0) return false;
	return 1<<($hochzahl);
}

function set_registerdepth($flagname,$depth){
	pg_exec($db," update wordflags set registerdepth=$depth where flagname='$flagname'");
}

function wordlist_cluster($db){
	pg_exec($db,"
		cluster wordflags_pkey on wordflags;
		cluster wordlink_pkey on wordlink;
		cluster wordcount_pkey on wordcount;
		cluster wordlist_wordnormlower_idx on wordlist;
	");
}

function wordlist_createtables($db, $datatablename, $datatable_pk_fieldname){
	pg_exec($db,"
		CREATE TABLE wordflags(
			flagbit integer PRIMARY KEY NOT NULL,
			flagname text NOT NULL,
			colname text NOT NULL,
			registerdepth integer default 2
		);
		
		create Table wordcount(
			wpk integer NOT NULL,
			flagbit integer NOT NULL,
			counter integer default 0,
			Primary Key (wpk,flagbit)
		);
		
		CREATE TABLE wordlist (
			pk serial PRIMARY KEY NOT NULL,
			word text NOT NULL,
			wordnorm text NOT NULL,
			wordnormlower text NOT NULL,
			l1 integer NOT NULL,
			l2 integer NOT NULL,
			flags integer
		);
		
		CREATE TABLE wordlink (
			wpk integer NOT NULL,
			pk integer NOT NULL references $datatablename($datatable_pk_fieldname) on delete cascade on update cascade,
			flags integer NOT NULL,
			Primary Key (wpk,pk)
		);
		
		create index wordlist_word_idx on wordlist using btree(word);
		create index wordlist_wordnorm_idx on wordlist using btree(wordnorm);
		create index wordlist_wordnormlower_idx on wordlist using btree(wordnormlower);
		
	");
	wordlist_cluster($db);
}


?>