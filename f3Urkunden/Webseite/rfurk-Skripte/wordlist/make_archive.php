#!/usr/bin/php 
<?


$dbname=$argv[1];

if (!$dbname) {
    echo "argv1=dbname\n";
    exit;
}

include_once("inc.pg.auth.php");



echo "Erstelle Tabellen...\n";

pg_exec("create table archive ( apk serial PRIMARY KEY, name text, count integer )");
pg_exec("create table archivelink ( apk integer references archive(apk), urknummer integer, primary key(apk,urknummer))");

echo "Fertig.\n\n";





echo "Erstelle Wortliste...\n";
$res=pg_exec($db,"select * from rfurk order by urknummer;");

while( $r = pg_fetch_array($res)) {
    $urknummer=$r["urknummer"];
    $ax=str_replace(array("\r\n","\n","\r"),"<br>",$r["archiv"]);
    foreach (explode("<br>",$ax) as $archivname) {
	if ($archivname!="") {
	    $res_archiv=pg_exec("select * from archive where name='$archivname'");
	    if ($r_archiv=pg_fetch_array($res_archiv)) {
		$newcount=$r_archiv["count"]+1;
		$apk=$r_archiv["apk"];
		pg_exec("update archive set count=".$newcount." where apk=".$apk);
		pg_exec("insert into archivelink (apk,urknummer) values ($apk, $urknummer)");
    		echo "Archiv gezählt: ".$archivname." ($newcount)\n";
	    } else {
		pg_exec("insert into archive (name, count) values('$archivname',1)"); 
		$res_apk=pg_exec("select currval('archive_apk_seq'::regclass)");
		$r_apk=pg_fetch_array($res_apk);
		$apk=$r_apk[0];
		pg_exec("insert into archivelink (apk,urknummer) values ($apk, $urknummer)");
    		echo "Archiv hinzugefügt: ".$archivname."\n";
	    }
	}
    }
}
echo "Cluster on Archive.";
pg_exec($db,"cluster archive_pkey on archive;");
pg_exec($db,"cluster archivelink_pkey on archivelink;");
pg_exec($db,"vacuum analyse archive");
pg_exec($db,"vacuum analyse archivelink");

?>
