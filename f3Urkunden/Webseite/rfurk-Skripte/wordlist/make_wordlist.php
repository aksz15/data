#!/usr/bin/php 
<?

include_once("inc_wordlist.php");

$dbname=$argv[1];

if (!$dbname) {
    echo "argv1=dbname\n";
    exit;
}

include_once("inc.pg.auth.php");


echo "Erstelle Tabellen...\n";
wordlist_createtables($db,"rfurk","lnr");
echo "Fertig.\n\n";




echo "Fülle Flagtabelle...\n";
$flags=array();
	//Flagname, columnname, registerdepth;
$flags[]=array("Ausstellungsort","ausstellungsort",1);
$flags[]=array("Kurzregest","kurzregest",2);
$flags[]=array("Archiv","archiv",1);
$flags[]=array("Reichsregister","rr",1);
$flags[]=array("Taxregister","taxreg",1);
$flags[]=array("Druckbeleg","druckbeleg",1);
$flags[]=array("Regestbeleg","regestbeleg",1);
$flags[]=array("Literatur","literatur",1);
$flags[]=array("Erwähnung","erwaehnung",1);
$flags[]=array("Empfänger","erwaehnung",1);
$flags[]=array("Region","region",1);
$flags[]=array("Orte","orte",1);
$flags[]=array("Sachbetreff","sachbetreff",1);
wordlist_setflags($db,$flags);
echo "Fertig.\n\n";


//readline("Return drücken zum weitermachen.");

echo "Erstelle Wortliste...\n";
$res=pg_exec($db,"select * from rfurk order by lnr;");

while( $r = pg_fetch_array($res)) {
	$lnr=$r["lnr"];
	if (($lnr % 1000)==1) echo $lnr."\n";
	#wordlist_addwords($db,$r["lnr"],"dfsdf sdfsdf 2000 fff",1);
	$flagnr=0;
	wordlist_addwords($db,$lnr,$r["ausstellungsort"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["kurzregest"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["archiv"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["rr"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["taxreg"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["druckbeleg"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["regestbeleg"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["literatur"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["erwaehnung"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["empfaenger"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["region"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["orte"],twopower($flagnr));
	$flagnr++;
	wordlist_addwords($db,$lnr,$r["sachbetreff"],twopower($flagnr));
}

wordlist_cluster($db);
pg_exec($db,"vacuum analyse");

?>
