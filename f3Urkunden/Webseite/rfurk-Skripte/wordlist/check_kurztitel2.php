#!/usr/bin/php 
<?

include_once("inc_wordlist.php");

$dbname=$argv[1];
$dbnameopac=$argv[2];

if (!$dbname) {
    echo "argv1=dbname\n";
    exit;
}

if (!$dbnameopac) {
    echo "argv1=dbname_opac\n";
    exit;
}


include_once("inc.pg.auth.php");


$connsql2='dbname='.$dbnameopac;
//echo $sql."\n";
$dbopac=pg_connect($connsql2);

global $cnt;
$cnt=0;

function kurztitel_ok(&$dbopac,&$kurztitel){
	$r=pg_exec($dbopac,"select * from kurztitel where kurztitel='$kurztitel'");
	if (pg_fetch_array($r)) {
		return true;
	}
	return false;
}

function check(&$db,&$dbopac,&$eintrag,&$info){
	$result=true;
	foreach(explode("<br>",$eintrag) as $e) {
		$kurztitel=$e;
		if (strpos($e,"/")>0) {
			$kurztitel=trim(substr($e,0,strpos($e,"/")));
		}
		if($kurztitel!="") {
			$ok=kurztitel_ok($dbopac,$kurztitel);
			if (!$ok) {
				echo "$e";
				echo "\n";
			}
		}
		
		//echo "<i>".$e."-".$k."</i><br>";
	}
	return $result;
}

$r=pg_exec($db,"select urknummer, index_datum, literatur, erwaehnung, regestbeleg, druckbeleg from rfurk");
while ($res=pg_fetch_array($r)){
	$fault=0;
	$info="      Literatur  ";
	$ok=check($db,$dbopac,$res["literatur"],$info);
	if (!$ok) $fault=1;
	$info="      Erwähnung  ";
	$ok=check($db,$dbopac,$res["erwaehnung"],$info);
	if (!$ok) $fault=1;
	$info="      Regestbeleg";
	$ok=check($db,$dbopac,$res["regestbeleg"],$info);
	if (!$ok) $fault=1;
	$info="      Druckbeleg ";
	$ok=check($db,$dbopac,$res["druckbeleg"],$info);
	if (!$ok) $fault=1;
}

echo "\n\nGesamtanzahl der Fehler: $cnt\n";


?>
