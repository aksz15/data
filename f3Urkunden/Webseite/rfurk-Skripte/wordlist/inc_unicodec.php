<?

function uc_utf8size($i){
  if ($i<0) return false;
  if ($i<=0x7f) return 1;
  if ($i<=0x7ff) return 2;
  if ($i<=0xffff) return 3;
  if ($i<=0x1fffff) return 4;
  if ($i<=0x3ffffff) return 5;
  return 6;
}

function str_utf8charsize($s,$pos=0){
	$o=ord($s[$pos]);
	if (($o & 0x80)==0) return 1;
	if (($o & 0xe0) == 0xc0) return 2;
	if (($o & 0xf0) == 0xe0) return 3;
	if (($o & 0xf8) == 0xf0) return 4;
	return false;
}



function i3u($i) { // returns UCS-16 or UCS-32 to UTF-8 from an integer
  $i=(int)$i; // integer?
  if ($i<0) return false; // positive?
  if ($i<=0x7f) return chr($i); // range 0
  if (($i & 0x7fffffff) <> $i) return '?'; // 31 bit?
  if ($i<=0x7ff) return chr(0xc0 | ($i >> 6)) . chr(0x80 | ($i & 0x3f));
  if ($i<=0xffff) return chr(0xe0 | ($i >> 12)) . chr(0x80 | ($i >> 6) & 0x3f)
      . chr(0x80  | $i & 0x3f);
  if ($i<=0x1fffff) return chr(0xf0 | ($i >> 18)) . chr(0x80 | ($i >> 12) & 0x3f)
      . chr(0x80 | ($i >> 6) & 0x3f) . chr(0x80  | $i & 0x3f);
  if ($i<=0x3ffffff) return chr(0xf8 | ($i >> 24)) . chr(0x80 | ($i >> 18) & 0x3f)
      . chr(0x80 | ($i >> 12) & 0x3f) . chr(0x80 | ($i >> 6) & 0x3f) . chr(0x80  | $i & 0x3f);
  return chr(0xfc | ($i >> 30)) . chr(0x80 | ($i >> 24) & 0x3f) . chr(0x80 | ($i >> 18) & 0x3f)
      . chr(0x80 | ($i >> 12) & 0x3f) . chr(0x80 | ($i >> 6) & 0x3f) . chr(0x80  | $i & 0x3f);
}

function u3i($s,$strict=1) { // returns integer on valid UTF-8 seq, NULL on empty, else FALSE
  // NOT strict: takes only DATA bits, present or not; strict: length and bits checking
  if ($s=='') return NULL;
  $l=strlen($s); $o=ord($s{0});
  if ($o <= 0x7f && $l==1) return $o;
  if ($l>6 && $strict) return false;
  if ($strict) for ($i=1;$i<$l;$i++) if (ord($s{$i}) > 0xbf || ord($s{$i})< 0x80) return false;
  if ($o < 0xc2) return false; // no-go even if strict=0
  if ($o <= 0xdf && ($l=2 && $strict)) return (($o & 0x1f) << 6 | (ord($s{1}) & 0x3f));
  if ($o <= 0xef && ($l=3 && $strict)) return (($o & 0x0f) << 12 | (ord($s{1}) & 0x3f) << 6
     |  (ord($s{2}) & 0x3f));
  if ($o <= 0xf7 && ($l=4 && $strict)) return (($o & 0x07) << 18 | (ord($s{1}) & 0x3f) << 12
     | (ord($s{2}) & 0x3f) << 6 |  (ord($s{3}) & 0x3f));
  if ($o <= 0xfb && ($l=5 && $strict)) return (($o & 0x03) << 24 | (ord($s{1}) & 0x3f) << 18
     | (ord($s{2}) & 0x3f) << 12 | (ord($s{3}) & 0x3f) << 6 |  (ord($s{4}) & 0x3f));
  if ($o <= 0xfd && ($l=6 && $strict)) return (($o & 0x01) << 30 | (ord($s{1}) & 0x3f) << 24
     | (ord($s{2}) & 0x3f) << 18 | (ord($s{3}) & 0x3f) << 12
     | (ord($s{4}) & 0x3f) << 6 |  (ord($s{5}) & 0x3f));
  return false;
}


function utf8_getchar($s,$pos,&$nextpos){
	$res=array();
	$size=strlen($s);
	if ($pos>=$size) return false;
	$cs=str_utf8charsize($s,$pos);
	$part=substr($s,$pos,$cs);
	$res=u3i($part);
	$nextpos+=$cs;
	return $res;
}


function utf8_getarray($s){
	$res=array();
	$size=strlen($s);
	$p=0;
	while ($p<$size) {
		$cs=str_utf8charsize($s,$p);
		$part=substr($s,$p,$cs);
		$res[]=u3i($part);
		$p+=$cs;
	}
	return $res;
}



function unicode_norm($letter){
if ($letter<128) return chr($letter);
switch ($letter) {
	case 146: return "";
	case 154: return "s";
	case 156: return "oe";
	case 158: return "z";
	case 170 : return 'a';	// ª -> a //Spanish male ordinal indicator
	case 186 : return 'o';	// º -> o //Spanish female ordinal indicator

	case 192: return "A";
	case 193: return "A";
	case 194: return "A";
	case 195: return "A";
	case 196: return "Ae"; //Ä
	case 197: return "A";
	case 198: return "AE"; //Ligatur
	case 199: return "C";
	case 200: return "E";
	case 201: return "E";
	case 202: return "E";
	case 203: return "E";
	case 204: return "I";
	case 205: return "I";
	case 206: return "I";
	case 207: return "I";
	case 208: return "D"; //Eth
	case 209: return "N";
	case 210: return "O";
	case 211: return "O";
	case 212: return "O";
	case 213: return "O";
	case 214: return "Oe"; //Ö
	case 216: return "Oe";  //Ø (enstspricht Ö)
	case 217: return "U";
	case 218: return "U";
	case 219: return "U";
	case 220: return "Ue";
	case 221: return "Y";
	case 222: return "Th"; //Þ = Thorn
	case 223: return "ss"; //ß
	case 224: return "a";
	case 225: return "a";
	case 226: return "a";
	case 227: return "a";
	case 228: return "ae"; //ä
	case 229: return "a";
	case 230: return "ae"; //Ligatur
	case 231: return "c";
	case 232: return "e";
	case 233: return "e";
	case 234: return "e";
	case 235: return "e";
	case 236: return "i";
	case 237: return "i";
	case 238: return "i";
	case 239: return "i";
	case 240: return "d"; //eth
	case 241: return "n";
	case 242: return "o";
	case 243: return "o";
	case 244: return "o";
	case 245: return "o";
	case 246: return "oe"; //ö
	case 248: return "oe";  //ø (Entspricht ö)
	case 249: return "u";
	case 250: return "u";
	case 251: return "u";
	case 252: return "ue"; //ü
	case 253: return "y";
	case 254: return "th"; //thorn
	case 255: return "y";
	//end of latin1;
	case 256: return "A";
	case 257: return "a";
	case 258: return "A";
	case 259: return "a";
	case 260: return "A";
	case 261: return "a";
	case 262: return "C";
	case 263: return "c";
	case 264: return "C";
	case 265: return "c";
	case 266: return "C";
	case 267: return "c";
	case 268: return "C";
	case 269: return "c";
	case 270: return "D";
	case 271: return "d";
	case 272: return "D";
	case 273: return "d";
	case 274: return "E";
	case 275: return "e";
	case 276: return "E";
	case 277: return "e";
	case 278: return "E";
	case 279: return "e";
	case 280: return "E";
	case 281: return "e";
	case 282: return "E";
	case 283: return "e";
	case 284: return "G";
	case 285: return "g";
	case 286: return "G";
	case 287: return "g";
	case 288: return "G";
	case 289: return "g";
	case 290: return "G";
	case 291: return "g";
	case 292: return "H";
	case 293: return "h";
	case 294: return "H";
	case 295: return "h";
	case 296: return "I";
	case 297: return "i";
	case 298: return "I";
	case 299: return "i";
	case 300: return "I";
	case 301: return "i";
	case 302: return "I";
	case 303: return "i";
	case 304: return "I";
	case 305: return "i";
	case 306: return "IJ"; //Ligatur
	case 307: return "ij"; // "
	case 308: return "J";
	case 309: return "j";
	case 310: return "K";
	case 311: return "k";
	case 312: return "q"; //Bei Rechtschreibreform durch q ersetzt, vorher "Kra" (ĸ)
	case 313: return "L"; //--Rest abwärts muss noch überprüft werden
	case 314: return "l";
	case 315: return "L";
	case 316: return "l";
	case 317: return "L";
	case 318: return "l";
	case 319: return "L";
	case 320: return "l";
	case 321: return "L";
	case 322: return "l";
	case 323: return "N";
	case 324: return "n";
	case 325: return "N";
	case 326: return "n";
	case 327: return "N";
	case 328: return "n";
	case 329: return "n";
	case 332: return "O";
	case 333: return "o";
	case 334: return "O";
	case 335: return "o";
	case 336: return "O";
	case 337: return "o";
	case 338: return "OE";
	case 339: return "oe";
	case 340: return "R";
	case 341: return "r";
	case 342: return "R";
	case 343: return "r";
	case 344: return "R";
	case 345: return "r";
	case 346: return "S";
	case 347: return "s";
	case 348: return "S";
	case 349: return "s";
	case 350: return "S";
	case 351: return "s";
	case 352: return "S";
	case 353: return "s";
	case 354: return "T";
	case 355: return "t";
	case 356: return "T";
	case 357: return "t";
	case 358: return "T";
	case 359: return "t";
	case 360: return "U";
	case 361: return "u";
	case 362: return "U";
	case 363: return "u";
	case 364: return "U";
	case 365: return "u";
	case 366: return "U";
	case 367: return "u";
	case 368: return "U";
	case 369: return "u";
	case 370: return "U";
	case 371: return "u";
	case 372: return "W";
	case 373: return "w";
	case 374: return "Y";
	case 375: return "y";
	case 376: return "Y";
	case 377: return "Z";
	case 378: return "z";
	case 379: return "Z";
	case 380: return "z";
	case 381: return "Z";
	case 382: return "z";
	case 383: return "s";
	case 384: return "b";
	case 385: return "B";
	case 386: return "B";
	case 387: return "b";
	case 388: return "?";
	case 389: return "?";
	case 390: return "O";
	case 391: return "C";
	case 392: return "C";
	case 393: return "D";
	case 394: return "D";
	case 395: return "D";
	case 396: return "d";
	case 397: return "d";
	case 398: return "E";
	case 399: return "e";
	case 400: return "E";
	case 401: return "F";
	case 402: return "f";
	case 403: return "G";
	case 404: return "y";
	case 405: return "hv";
	case 406: return "I";
	case 407: return "I";
	case 408: return "K";
	case 409: return "k";
	case 410: return "l";
	case 411: return "l";
	case 412: return "M";
	case 413: return "N";
	case 414: return "n";
	case 415: return "O";
	case 416: return "O";
	case 417: return "o";
	case 418: return "O";
	case 419: return "o";
	case 420: return "P";
	case 421: return "p";
	case 422: return "y";
	case 423: return "2";
	case 424: return "2";
	case 427: return "t";
	case 428: return "T";
	case 429: return "t";
	case 430: return "T";
	case 431: return "U";
	case 432: return "u";
	case 433: return "U";
	case 434: return "V";
	case 435: return "Y";
	case 436: return "y";
	case 437: return "Z";
	case 438: return "z";
	case 443: return "2";
	case 444: return "5";
	case 445: return "5";
	case 452: return "DZ";
	case 453: return "Dz";
	case 454: return "dz";
	case 455: return "LJ";
	case 456: return "Lj";
	case 457: return "lj";
	case 458: return "NJ";
	case 459: return "Nj";
	case 460: return "nj";
	case 461: return "A";
	case 462: return "a";
	case 463: return "I";
	case 464: return "i";
	case 465: return "O";
	case 466: return "o";
	case 467: return "U";
	case 468: return "u";
	case 469: return "U";
	case 470: return "u";
	case 471: return "U";
	case 472: return "u";
	case 473: return "U";
	case 474: return "u";
	case 475: return "U";
	case 476: return "u";
	case 477: return "e";
	case 478: return "A";
	case 479: return "a";
	case 480: return "A";
	case 481: return "a";
	case 482: return "A";
	case 483: return "a";
	case 484: return "G";
	case 485: return "g";
	case 486: return "G";
	case 487: return "g";
	case 488: return "K";
	case 489: return "k";
	case 490: return "O";
	case 491: return "o";
	case 492: return "O";
	case 493: return "o";
	case 496: return "j";
	case 497: return "DZ";
	case 498: return "Dz";
	case 499: return "dz";
	case 500: return "G";
	case 501: return "g";
	case 504: return "N";
	case 505: return "n";
	case 506: return "A";
	case 507: return "A";
	case 508: return "AE";
	case 509: return "ae";
	case 510: return "O";
	case 511: return "o";
	case 512: return "A";
	case 513: return "a";
	case 514: return "A";
	case 515: return "a";
	case 516: return "E";
	case 517: return "e";
	case 518: return "E";
	case 519: return "e";
	case 520: return "I";
	case 521: return "i";
	case 522: return "I";
	case 523: return "i";
	case 524: return "O";
	case 525: return "o";
	case 526: return "O";
	case 527: return "o";
	case 528: return "R";
	case 529: return "r";
	case 530: return "R";
	case 531: return "r";
	case 532: return "U";
	case 533: return "u";
	case 534: return "U";
	case 535: return "u";
	case 536: return "S";
	case 537: return "s";
	case 538: return "T";
	case 539: return "t";
	case 542: return "H";
	case 543: return "h";
	case 544: return "N";
	case 545: return "d";
	case 546: return "OU";
	case 547: return "ou";
	case 548: return "Z";
	case 549: return "z";
	case 550: return "A";
	case 551: return "a";
	case 552: return "E";
	case 553: return "e";
	case 554: return "O";
	case 555: return "o";
	case 556: return "O";
	case 557: return "o";
	case 558: return "O";
	case 559: return "o";
	case 560: return "O";
	case 561: return "o";
	case 562: return "Y";
	case 563: return "y";
	case 564: return "l";
	case 565: return "n";
	case 566: return "r";
	case 567: return "j";
	case 568: return "db";
	case 569: return "qp";
	case 570: return "A";
	case 571: return "C";
	case 572: return "c";
	case 573: return "L";
	case 574: return "T";
	case 575: return "s";
	case 576: return "z";
	case 579: return "B";
	case 580: return "U";
	case 581: return "V";
	case 582: return "E";
	case 583: return "e";
	case 584: return "J";
	case 585: return "j";
	case 586: return "q";
	case 587: return "q";
	case 588: return "R";
	case 589: return "r";
	case 590: return "Y";
	case 591: return "y";
	case 592: return "a";
	case 593: return "a";
	case 594: return "a";
	case 595: return "b";
	case 596: return "o";
	case 597: return "c";
	case 598: return "d";
	case 599: return "d";
	case 600: return "e";
	case 603: return "e";
	case 604: return "e";
	case 605: return "e";
	case 606: return "e";
	case 607: return "j";
	case 608: return "g";
	case 609: return "g";
	case 610: return "G";
	case 613: return "h";
	case 614: return "h";
	case 616: return "i";
	case 618: return "i";
	case 619: return "l";
	case 620: return "l";
	case 621: return "l";
	case 623: return "m";
	case 624: return "m";
	case 625: return "m";
	case 626: return "n";
	case 627: return "n";
	case 628: return "n";
	case 629: return "o";
	case 630: return "oe";
	case 633: return "r";
	case 634: return "r";
	case 635: return "r";
	case 636: return "r";
	case 637: return "r";
	case 638: return "r";
	case 639: return "r";
	case 640: return "R";
	case 641: return "r";
	case 642: return "s";
	case 644: return "j";
	case 647: return "t";
	case 648: return "t";
	case 649: return "u";
	case 651: return "v";
	case 652: return "v";
	case 653: return "w";
	case 654: return "y";
	case 655: return "y";
	case 656: return "z";
	case 657: return "z";
	case 663: return "C";
	case 665: return "b";
	case 666: return "e";
	case 667: return "g";
	case 668: return "h";
	case 669: return "j";
	case 670: return "k";
	case 671: return "l";
	case 672: return "q";
	case 675: return "dz";
	case 677: return "dz";
	case 678: return "ts";
	case 680: return "tc";
	case 682: return "ls";
	case 683: return "lz";
	case 686: return "h";
	case 687: return "h";
	default:
		return "";
	}

}


function utf8_normalize($s){
	$res="";
	$size=strlen($s);
	$p=0;
	global $normerrorlist;
	while ($p<$size) {
		$cs=str_utf8charsize($s,$p);
		$part=substr($s,$p,$cs);
		$norm=unicode_norm(u3i($part));
		if ($norm=="") {
		    $norm=$part;
		    $normerrorlist[$part]=1;
		}
		$res.=$norm;
		$p+=$cs;
	}
	return $res;
}



function unicode_norm_register($letter){
if (($letter>=65) and ($letter<=90)) return $letter+32;
if ($letter<128) return $letter;
switch ($letter) {
	case 181 : return 121;	// µ -> y //??? vielleicht "m"
	case 186 : return 111;	// º -> o //Spanish female ordinal indicator
	case 170 : return 97;	// ª -> a //Spanish male ordinal indicator
	case 192 : return 97;	// À -> a
	case 193 : return 97;	// Á -> a
	case 194 : return 97;	// Â -> a
	case 195 : return 97;	// Ã -> a
	case 196 : return 97;	// Ä -> a
	case 197 : return 97;	// Å -> a
	case 198 : return 97;	// Æ -> a
	case 199 : return 99;	// Ç -> c
	case 200 : return 101;	// È -> e
	case 201 : return 101;	// É -> e
	case 202 : return 101;	// Ê -> e
	case 203 : return 101;	// Ë -> e
	case 204 : return 105;	// Ì -> i
	case 205 : return 105;	// Í -> i
	case 206 : return 105;	// Î -> i
	case 207 : return 105;	// Ï -> i
	case 208 : return 100;	// Ð -> d
	case 209 : return 110;	// Ñ -> n
	case 210 : return 111;	// Ò -> o
	case 211 : return 111;	// Ó -> o
	case 212 : return 111;	// Ô -> o
	case 213 : return 111;	// Õ -> o
	case 214 : return 111;	// Ö -> o
	case 216 : return 111;	// Ø -> o
	case 217 : return 111;	// Ù -> o
	case 218 : return 111;	// Ú -> o
	case 219 : return 117;	// Û -> u
	case 220 : return 117;	// Ü -> u
	case 221 : return 121;	// Ý -> y
	case 223 : return 115;	// ß -> s
	case 224 : return 97;	// à -> a
	case 225 : return 97;	// á -> a
	case 226 : return 97;	// â -> a
	case 227 : return 97;	// ã -> a
	case 228 : return 97;	// ä -> a
	case 229 : return 97;	// å -> a
	case 230 : return 97;	// æ -> a
	case 231 : return 99;	// ç -> c
	case 232 : return 101;	// è -> e
	case 233 : return 101;	// é -> e
	case 234 : return 101;	// ê -> e
	case 235 : return 101;	// ë -> e
	case 236 : return 105;	// ì -> i
	case 237 : return 105;	// í -> i
	case 238 : return 105;	// î -> i
	case 239 : return 105;	// ï -> i
	case 240 : return 100;	// ð -> d
	case 241 : return 110;	// ñ -> n
	case 242 : return 111;	// ò -> o
	case 243 : return 111;	// ó -> o
	case 244 : return 111;	// ô -> o
	case 245 : return 111;	// õ -> o
	case 246 : return 111;	// ö -> o
	case 248 : return 111;	// ø -> o
	case 249 : return 117;	// ù -> u
	case 250 : return 117;	// ú -> u
	case 251 : return 117;	// û -> u
	case 252 : return 117;	// ü -> u
	case 253 : return 121;	// ý -> y
	case 255 : return 121;	// ÿ -> y
	case 256 : return 97;	// Ā -> a
	case 257 : return 97;	// ā -> a
	case 258 : return 97;	// Ă -> a
	case 259 : return 97;	// ă -> a
	case 260 : return 97;	// Ą -> a
	case 261 : return 97;	// ą -> a
	case 262 : return 99;	// Ć -> c
	case 263 : return 99;	// ć -> c
	case 264 : return 99;	// Ĉ -> c
	case 265 : return 99;	// ĉ -> c
	case 266 : return 99;	// Ċ -> c
	case 267 : return 99;	// ċ -> c
	case 268 : return 99;	// Č -> c
	case 269 : return 99;	// č -> c
	case 270 : return 100;	// Ď -> d
	case 271 : return 100;	// ď -> d
	case 272 : return 100;	// Đ -> d
	case 273 : return 100;	// đ -> d
	case 274 : return 101;	// Ē -> e
	case 275 : return 101;	// ē -> e
	case 276 : return 101;	// Ĕ -> e
	case 277 : return 101;	// ĕ -> e
	case 278 : return 101;	// Ė -> e
	case 279 : return 101;	// ė -> e
	case 280 : return 101;	// Ę -> e
	case 281 : return 101;	// ę -> e
	case 282 : return 101;	// Ě -> e
	case 283 : return 101;	// ě -> e
	case 284 : return 103;	// Ĝ -> g
	case 285 : return 103;	// ĝ -> g
	case 286 : return 103;	// Ğ -> g
	case 287 : return 103;	// ğ -> g
	case 288 : return 103;	// Ġ -> g
	case 289 : return 103;	// ġ -> g
	case 290 : return 103;	// Ģ -> g
	case 291 : return 103;	// ģ -> g
	case 292 : return 104;	// Ĥ -> h
	case 293 : return 104;	// ĥ -> h
	case 294 : return 104;	// Ħ -> h
	case 295 : return 104;	// ħ -> h
	case 296 : return 105;	// Ĩ -> i
	case 297 : return 105;	// ĩ -> i
	case 298 : return 105;	// Ī -> i
	case 299 : return 105;	// ī -> i
	case 300 : return 105;	// Ĭ -> i
	case 301 : return 105;	// ĭ -> i
	case 302 : return 105;	// Į -> i
	case 303 : return 105;	// į -> i
	case 304 : return 105;	// İ -> i
	case 305 : return 105;	// ı -> i
	case 306 : return 105;	// Ĳ -> i
	case 307 : return 105;	// ĳ -> i
	case 308 : return 106;	// Ĵ -> j
	case 309 : return 106;	// ĵ -> j
	case 310 : return 107;	// Ķ -> k
	case 311 : return 107;	// ķ -> k
	case 313 : return 108;	// Ĺ -> l
	case 314 : return 108;	// ĺ -> l
	case 315 : return 108;	// Ļ -> l
	case 316 : return 108;	// ļ -> l
	case 317 : return 108;	// Ľ -> l
	case 318 : return 108;	// ľ -> l
	case 319 : return 108;	// Ŀ -> l
	case 320 : return 108;	// ŀ -> l
	case 321 : return 108;	// Ł -> l
	case 322 : return 108;	// ł -> l
	case 323 : return 110;	// Ń -> n
	case 324 : return 110;	// ń -> n
	case 325 : return 110;	// Ņ -> n
	case 326 : return 110;	// ņ -> n
	case 327 : return 110;	// Ň -> n
	case 328 : return 110;	// ň -> n
	case 329 : return 110;	// ŉ -> n
	case 332 : return 111;	// Ō -> o
	case 333 : return 111;	// ō -> o
	case 334 : return 111;	// Ŏ -> o
	case 335 : return 111;	// ŏ -> o
	case 336 : return 111;	// Ő -> o
	case 337 : return 111;	// ő -> o
	case 338 : return 111;	// Œ -> o
	case 339 : return 111;	// œ -> o
	case 340 : return 114;	// Ŕ -> r
	case 341 : return 114;	// ŕ -> r
	case 342 : return 114;	// Ŗ -> r
	case 343 : return 114;	// ŗ -> r
	case 344 : return 114;	// Ř -> r
	case 345 : return 114;	// ř -> r
	case 346 : return 115;	// Ś -> s
	case 347 : return 115;	// ś -> s
	case 348 : return 115;	// Ŝ -> s
	case 349 : return 115;	// ŝ -> s
	case 350 : return 115;	// Ş -> s
	case 351 : return 115;	// ş -> s
	case 352 : return 115;	// Š -> s
	case 353 : return 115;	// š -> s
	case 354 : return 116;	// Ţ -> t
	case 355 : return 116;	// ţ -> t
	case 356 : return 116;	// Ť -> t
	case 357 : return 116;	// ť -> t
	case 358 : return 116;	// Ŧ -> t
	case 359 : return 116;	// ŧ -> t
	case 360 : return 117;	// Ũ -> u
	case 361 : return 117;	// ũ -> u
	case 362 : return 117;	// Ū -> u
	case 363 : return 117;	// ū -> u
	case 364 : return 117;	// Ŭ -> u
	case 365 : return 117;	// ŭ -> u
	case 366 : return 117;	// Ů -> u
	case 367 : return 117;	// ů -> u
	case 368 : return 117;	// Ű -> u
	case 369 : return 117;	// ű -> u
	case 370 : return 117;	// Ų -> u
	case 371 : return 117;	// ų -> u
	case 372 : return 119;	// Ŵ -> w
	case 373 : return 119;	// ŵ -> w
	case 374 : return 121;	// Ŷ -> y
	case 375 : return 121;	// ŷ -> y
	case 376 : return 121;	// Ÿ -> y
	case 377 : return 122;	// Ź -> z
	case 378 : return 122;	// ź -> z
	case 379 : return 122;	// Ż -> z
	case 380 : return 122;	// ż -> z
	case 381 : return 122;	// Ž -> z
	case 382 : return 122;	// ž -> z
	case 383 : return 115;	// ſ -> s
	case 384 : return 98;	// ƀ -> b
	case 385 : return 98;	// Ɓ -> b
	case 386 : return 98;	// Ƃ -> b
	case 387 : return 98;	// ƃ -> b
	case 388 : return 63;	// Ƅ -> ?
	case 389 : return 63;	// ƅ -> ?
	case 390 : return 111;	// Ɔ -> o
	case 391 : return 99;	// Ƈ -> c
	case 392 : return 99;	// ƈ -> c
	case 393 : return 100;	// Ɖ -> d
	case 394 : return 100;	// Ɗ -> d
	case 395 : return 100;	// Ƌ -> d
	case 396 : return 100;	// ƌ -> d
	case 397 : return 100;	// ƍ -> d
	case 398 : return 101;	// Ǝ -> e
	case 399 : return 101;	// Ə -> e
	case 400 : return 101;	// Ɛ -> e
	case 401 : return 102;	// Ƒ -> f
	case 402 : return 102;	// ƒ -> f
	case 403 : return 103;	// Ɠ -> g
	case 404 : return 121;	// Ɣ -> y
	case 405 : return 104;	// ƕ -> h
	case 406 : return 105;	// Ɩ -> i
	case 407 : return 105;	// Ɨ -> i
	case 408 : return 107;	// Ƙ -> k
	case 409 : return 107;	// ƙ -> k
	case 410 : return 108;	// ƚ -> l
	case 411 : return 108;	// ƛ -> l
	case 412 : return 109;	// Ɯ -> m
	case 413 : return 110;	// Ɲ -> n
	case 414 : return 110;	// ƞ -> n
	case 415 : return 111;	// Ɵ -> o
	case 416 : return 111;	// Ơ -> o
	case 417 : return 111;	// ơ -> o
	case 418 : return 111;	// Ƣ -> o
	case 419 : return 111;	// ƣ -> o
	case 420 : return 112;	// Ƥ -> p
	case 421 : return 112;	// ƥ -> p
	case 422 : return 121;	// Ʀ -> y
	case 423 : return 50;	// Ƨ -> 2
	case 424 : return 50;	// ƨ -> 2
	case 427 : return 116;	// ƫ -> t
	case 428 : return 116;	// Ƭ -> t
	case 429 : return 116;	// ƭ -> t
	case 430 : return 116;	// Ʈ -> t
	case 431 : return 117;	// Ư -> u
	case 432 : return 117;	// ư -> u
	case 433 : return 117;	// Ʊ -> u
	case 434 : return 118;	// Ʋ -> v
	case 435 : return 121;	// Ƴ -> y
	case 436 : return 121;	// ƴ -> y
	case 437 : return 122;	// Ƶ -> z
	case 438 : return 122;	// ƶ -> z
	case 443 : return 50;	// ƻ -> 2
	case 444 : return 53;	// Ƽ -> 5
	case 445 : return 53;	// ƽ -> 5
	case 452 : return 100;	// Ǆ -> d
	case 453 : return 100;	// ǅ -> d
	case 454 : return 100;	// ǆ -> d
	case 455 : return 108;	// Ǉ -> l
	case 456 : return 108;	// ǈ -> l
	case 457 : return 108;	// ǉ -> l
	case 458 : return 110;	// Ǌ -> n
	case 459 : return 110;	// ǋ -> n
	case 460 : return 110;	// ǌ -> n
	case 461 : return 97;	// Ǎ -> a
	case 462 : return 97;	// ǎ -> a
	case 463 : return 105;	// Ǐ -> i
	case 464 : return 105;	// ǐ -> i
	case 465 : return 111;	// Ǒ -> o
	case 466 : return 111;	// ǒ -> o
	case 467 : return 117;	// Ǔ -> u
	case 468 : return 117;	// ǔ -> u
	case 469 : return 117;	// Ǖ -> u
	case 470 : return 117;	// ǖ -> u
	case 471 : return 117;	// Ǘ -> u
	case 472 : return 117;	// ǘ -> u
	case 473 : return 117;	// Ǚ -> u
	case 474 : return 117;	// ǚ -> u
	case 475 : return 117;	// Ǜ -> u
	case 476 : return 117;	// ǜ -> u
	case 477 : return 101;	// ǝ -> e
	case 478 : return 97;	// Ǟ -> a
	case 479 : return 97;	// ǟ -> a
	case 480 : return 97;	// Ǡ -> a
	case 481 : return 97;	// ǡ -> a
	case 482 : return 97;	// Ǣ -> a
	case 483 : return 97;	// ǣ -> a
	case 484 : return 103;	// Ǥ -> g
	case 485 : return 103;	// ǥ -> g
	case 486 : return 103;	// Ǧ -> g
	case 487 : return 103;	// ǧ -> g
	case 488 : return 107;	// Ǩ -> k
	case 489 : return 107;	// ǩ -> k
	case 490 : return 111;	// Ǫ -> o
	case 491 : return 111;	// ǫ -> o
	case 492 : return 111;	// Ǭ -> o
	case 493 : return 111;	// ǭ -> o
	case 496 : return 106;	// ǰ -> j
	case 497 : return 100;	// Ǳ -> d
	case 498 : return 100;	// ǲ -> d
	case 499 : return 100;	// ǳ -> d
	case 500 : return 103;	// Ǵ -> g
	case 501 : return 103;	// ǵ -> g
	case 504 : return 110;	// Ǹ -> n
	case 505 : return 110;	// ǹ -> n
	case 506 : return 97;	// Ǻ -> a
	case 507 : return 97;	// ǻ -> a
	case 508 : return 97;	// Ǽ -> a
	case 509 : return 97;	// ǽ -> a
	case 510 : return 111;	// Ǿ -> o
	case 511 : return 111;	// ǿ -> o
	case 512 : return 97;	// Ȁ -> a
	case 513 : return 97;	// ȁ -> a
	case 514 : return 97;	// Ȃ -> a
	case 515 : return 97;	// ȃ -> a
	case 516 : return 101;	// Ȅ -> e
	case 517 : return 101;	// ȅ -> e
	case 518 : return 101;	// Ȇ -> e
	case 519 : return 101;	// ȇ -> e
	case 520 : return 105;	// Ȉ -> i
	case 521 : return 105;	// ȉ -> i
	case 522 : return 105;	// Ȋ -> i
	case 523 : return 105;	// ȋ -> i
	case 524 : return 111;	// Ȍ -> o
	case 525 : return 111;	// ȍ -> o
	case 526 : return 111;	// Ȏ -> o
	case 527 : return 111;	// ȏ -> o
	case 528 : return 114;	// Ȑ -> r
	case 529 : return 114;	// ȑ -> r
	case 530 : return 114;	// Ȓ -> r
	case 531 : return 114;	// ȓ -> r
	case 532 : return 117;	// Ȕ -> u
	case 533 : return 117;	// ȕ -> u
	case 534 : return 117;	// Ȗ -> u
	case 535 : return 117;	// ȗ -> u
	case 536 : return 115;	// Ș -> s
	case 537 : return 115;	// ș -> s
	case 538 : return 116;	// Ț -> t
	case 539 : return 116;	// ț -> t
	case 542 : return 104;	// Ȟ -> h
	case 543 : return 104;	// ȟ -> h
	case 544 : return 110;	// Ƞ -> n
	case 545 : return 100;	// ȡ -> d
	case 546 : return 111;	// Ȣ -> o
	case 547 : return 111;	// ȣ -> o
	case 548 : return 122;	// Ȥ -> z
	case 549 : return 122;	// ȥ -> z
	case 550 : return 97;	// Ȧ -> a
	case 551 : return 97;	// ȧ -> a
	case 552 : return 101;	// Ȩ -> e
	case 553 : return 101;	// ȩ -> e
	case 554 : return 111;	// Ȫ -> o
	case 555 : return 111;	// ȫ -> o
	case 556 : return 111;	// Ȭ -> o
	case 557 : return 111;	// ȭ -> o
	case 558 : return 111;	// Ȯ -> o
	case 559 : return 111;	// ȯ -> o
	case 560 : return 111;	// Ȱ -> o
	case 561 : return 111;	// ȱ -> o
	case 562 : return 121;	// Ȳ -> y
	case 563 : return 121;	// ȳ -> y
	case 564 : return 108;	// ȴ -> l
	case 565 : return 110;	// ȵ -> n
	case 566 : return 114;	// ȶ -> r
	case 567 : return 106;	// ȷ -> j
	case 568 : return 100;	// ȸ -> d
	case 569 : return 113;	// ȹ -> q
	case 570 : return 97;	// Ⱥ -> a
	case 571 : return 99;	// Ȼ -> c
	case 572 : return 99;	// ȼ -> c
	case 573 : return 108;	// Ƚ -> l
	case 574 : return 116;	// Ⱦ -> t
	case 575 : return 115;	// ȿ -> s
	case 576 : return 122;	// ɀ -> z
	case 579 : return 98;	// Ƀ -> b
	case 580 : return 117;	// Ʉ -> u
	case 581 : return 118;	// Ʌ -> v
	case 582 : return 101;	// Ɇ -> e
	case 583 : return 101;	// ɇ -> e
	case 584 : return 106;	// Ɉ -> j
	case 585 : return 106;	// ɉ -> j
	case 586 : return 113;	// Ɋ -> q
	case 587 : return 113;	// ɋ -> q
	case 588 : return 114;	// Ɍ -> r
	case 589 : return 114;	// ɍ -> r
	case 590 : return 121;	// Ɏ -> y
	case 591 : return 121;	// ɏ -> y
	case 592 : return 97;	// ɐ -> a
	case 593 : return 97;	// ɑ -> a
	case 594 : return 97;	// ɒ -> a
	case 595 : return 98;	// ɓ -> b
	case 596 : return 111;	// ɔ -> o
	case 597 : return 99;	// ɕ -> c
	case 598 : return 100;	// ɖ -> d
	case 599 : return 100;	// ɗ -> d
	case 600 : return 101;	// ɘ -> e
	case 603 : return 101;	// ɛ -> e
	case 604 : return 101;	// ɜ -> e
	case 605 : return 101;	// ɝ -> e
	case 606 : return 101;	// ɞ -> e
	case 607 : return 106;	// ɟ -> j
	case 608 : return 103;	// ɠ -> g
	case 609 : return 103;	// ɡ -> g
	case 610 : return 103;	// ɢ -> g
	case 613 : return 104;	// ɥ -> h
	case 614 : return 104;	// ɦ -> h
	case 616 : return 105;	// ɨ -> i
	case 618 : return 105;	// ɪ -> i
	case 619 : return 108;	// ɫ -> l
	case 620 : return 108;	// ɬ -> l
	case 621 : return 108;	// ɭ -> l
	case 623 : return 109;	// ɯ -> m
	case 624 : return 109;	// ɰ -> m
	case 625 : return 109;	// ɱ -> m
	case 626 : return 110;	// ɲ -> n
	case 627 : return 110;	// ɳ -> n
	case 628 : return 110;	// ɴ -> n
	case 629 : return 111;	// ɵ -> o
	case 630 : return 111;	// ɶ -> o
	case 633 : return 114;	// ɹ -> r
	case 634 : return 114;	// ɺ -> r
	case 635 : return 114;	// ɻ -> r
	case 636 : return 114;	// ɼ -> r
	case 637 : return 114;	// ɽ -> r
	case 638 : return 114;	// ɾ -> r
	case 639 : return 114;	// ɿ -> r
	case 640 : return 114;	// ʀ -> r
	case 641 : return 114;	// ʁ -> r
	case 642 : return 115;	// ʂ -> s
	case 644 : return 106;	// ʄ -> j
	case 647 : return 116;	// ʇ -> t
	case 648 : return 116;	// ʈ -> t
	case 649 : return 117;	// ʉ -> u
	case 651 : return 118;	// ʋ -> v
	case 652 : return 118;	// ʌ -> v
	case 653 : return 119;	// ʍ -> w
	case 654 : return 121;	// ʎ -> y
	case 655 : return 121;	// ʏ -> y
	case 656 : return 122;	// ʐ -> z
	case 657 : return 122;	// ʑ -> z
	case 663 : return 99;	// ʗ -> c
	case 665 : return 98;	// ʙ -> b
	case 666 : return 101;	// ʚ -> e
	case 667 : return 103;	// ʛ -> g
	case 668 : return 104;	// ʜ -> h
	case 669 : return 106;	// ʝ -> j
	case 670 : return 107;	// ʞ -> k
	case 671 : return 108;	// ʟ -> l
	case 672 : return 113;	// ʠ -> q
	case 675 : return 100;	// ʣ -> d
	case 677 : return 100;	// ʥ -> d
	case 678 : return 116;	// ʦ -> t
	case 680 : return 116;	// ʨ -> t
	case 682 : return 108;	// ʪ -> l
	case 683 : return 108;	// ʫ -> l
	case 686 : return 104;	// ʮ -> h
	case 687 : return 104;	// ʯ -> h

	default:
		return $letter;
	}

}


function utf8_register_letters($s){
	$res=array();
	$size=strlen($s);
	$p=0;
	$lcnt=0;
	while (($p<$size) and ($lcnt<2)) {
		$cs=str_utf8charsize($s,$p);
		$part=substr($s,$p,$cs);
		$norm=unicode_norm_register(u3i($part));
		$res[]=$norm;
		$p+=$cs;
	}
	for ($i=$lcnt; $i<2; $i++) $res[]=0;
	return $res;
}

function utf8_normalize_register($s){
	$res="";
	$size=strlen($s);
	$p=0;
	while ($p<$size) {
		$cs=str_utf8charsize($s,$p);
		$part=substr($s,$p,$cs);
		$norm=i3u(unicode_norm_register(u3i($part)));
		if ($norm=="") {
		    $norm=$part;
		}
		$res.=$norm;
		$p+=$cs;
	}
	return $res;
}

?>
