CREATE TABLE wordflags(
	flagpk integer PRIMARY KEY NOT NULL;
	flagname text NOT NULL;
	flaginfo text;
	flagwordcount integer;
);

create Table wordcount(
	wpk integer NOT NULL;
	flagpk integer NOT NULL;
	wordcount integer NOT NULL;
);

CREATE TABLE wordlist (
    pk serial PRIMARY KEY NOT NULL,
    word text NOT NULL,
    wordnorm text NOT NULL,
    l1 integer NOT NULL,
    l2 integer NOT NULL,
    flags integer
);

CREATE TABLE wordlink (
    wpk integer NOT NULL,
    pk integer NOT NULL,
    flags integer
);



