ALTER TABLE ONLY rfurk
    ADD CONSTRAINT rfurk_pkey PRIMARY KEY (lnr);

cluster rfurk_pkey on rfurk;

CREATE INDEX rfurk_ausstellungsort_idx ON rfurk USING btree (ausstellungsort);
CREATE INDEX rfurk_jahr_idx ON rfurk USING btree (jahr);
CREATE INDEX rfurk_monat_idx ON rfurk USING btree (monat);
CREATE INDEX rfurk_tag_idx ON rfurk USING btree (tag);
CREATE INDEX rfurk_urknummer_idx ON rfurk USING btree (urknummer);
CREATE INDEX rfurk_urkunden_nummer_idx ON rfurk USING btree (urkunden_nummer);
