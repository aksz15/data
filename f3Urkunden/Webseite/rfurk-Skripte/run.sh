if which php >/dev/null; then
    echo "Benoetigtes Programm 'php' existiert."
else
  echo "PROGRAMM NICHT VORHANDEN:"
  echo "php (Als Admin mit 'apt-get php-cli' Paket installieren)"
  exit
fi

if which psql >/dev/null; then
    echo "Benoetigtes Programm 'psql' existiert."
else
  echo "PROGRAMM NICHT VORHANDEN:"
  echo "psql / postgres-client (Als Admin Paket installieren)"
  exit
fi

if which createdb >/dev/null; then
    echo "Benoetigtes Programm 'createdb' existiert."
else
  echo "PROGRAMM NICHT VORHANDEN:"
  echo "createdb / postgres-client (Als Admin Paket installieren)"
  exit
fi

FAUSTEXPORT="export/export.xml"

if [ ! -f "$FAUSTEXPORT" ]
then
  echo "DATEI NICHT VORHANDEN:"
  echo "Benötige Faust-Export-Datei '$FAUSTEXPORT' wurde nicht gefunden."
  exit
fi

if [ $1 ]; then
    echo "Erstelle Friedrich-Urkunden-Datenbank: $1"
else
    echo "***Friedrich-Urkunden Datenbank erstellen***"
    echo "Start: run.sh DATENBANKNAME"
    exit
fi



./0_convert.sh

./1_makedb.sh $1

