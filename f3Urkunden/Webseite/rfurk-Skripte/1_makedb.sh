createdb $1
psql -f "export/rfurk_table.sql" $1
psql -f DB/rfurk_optimize.sql $1
psql -f DB/orte.sql $1
cd wordlist
php -f make_wordlist.php $1
php -f make_archive.php $1
cd ..
psql -f DB/grants_reader.sql $1
psql -c "vacuum analyse;" $1
