# Datenbeschreibung


| Dateiname | Beschreibung |
| ------ | ------ |
|deskriptoren.csv||
|ejahr.csv||
|index_gesamttitel.csv||
|index_reihe.csv||
|index_sammelwerk.csv||
|index_zeitschrift.csv||
|kurztitel.csv||
|links_gesamttitel.csv||
|links_reihe.csv||
|links_sammelwerk.csv||
|links_zeitschrift.csv||
|register_kurztitel.csv||
|register_personen.csv||
|register_personen_links.csv||
|systhes2.csv||
|test.csv||
|werke.csv||
|werke1.csv||
|werke2.csv||
|werke3.csv||
|werke4.csv||
|werke5.csv||
|werke6.csv||
|werke7.csv||
|werke8.csv||

|Objektartnummer|Vereinheitlichung|Beschreibung de | Beschreibung en | Bemerkungen | Bibtex | Wikidata
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |  
|1|Verfasserwerk|Buch|Book|Eigenständiges Werk|@book|Q571|
|2|Sachtitelwerk|Sachtitelwerk|Book|Buch ggf. ohne Verfasser (z.B. Quelleneditionen), kann Teil von Gesamtwerk sein.|@book |Q571|
|3|Sammelwerk|Buch|Book|Buch als Teil einer Reihe|@book|Q571|
|4|Gesamtwerk|Gesamtwerk|Sammlung von Büchern, z.B. Sachtitelwerken|1,2,3 können Teil eines Gesamtwerks sein|@collection|Q20655472|
|5|Reihe|Reihe|Proceedings|1,2,3,4 können Teil einer Reihe sein|@proceedings| Q1143604|
|6|Zeitschrift|Aufsatz|Journal|Zeitschrift, die Aufsätze(8) enthält|@periodical| Q1002697|
|7|Buchbeitrag|Buchbeitrag|Article|Aufsatz in einem Sammelwerk, die Property sammelwerktitel enthält den Titel des Sammelwerks(3)|@article|Q13442814|
|8|Aufsatz/Zeitschriftenbeitrag|Aufsatz|Article|Aufsatz in einer Zeitschrift(6)|@article|Q13442814|

### Diese Beziehungen gibt es in den Daten


```
match (w1:Werk)-[r]->(w2:Werk)
return distinct labels(w1), w1.objektart, type(r), labels(w2), w2.objektart;
```

|labels(w1)|w1.objektart|type(r)|labels(w2)|w2.objektart
|---|---|----|---|---|
|Aufsatz|8|IST_TEIL_VON|Zeitschrift|6|
|Monographie|1|IST_TEIL_VON|Reihe|5|
|Sammelwerk|3|IST_TEIL_VON|Reihe|5|
|Buchbeitrag|7|IST_TEIL_VON|Sammelwerk|3|
|Sachtitelwerk|2|IST_TEIL_VON|Reihe|5|
|Gesamtwerk|4|IST_TEIL_VON|Reihe|5|
|Sachtitelwerk|2|IST_TEIL_VON|Gesamtwerk|4|
|Sammelwerk|3|IST_TEIL_VON|Gesamtwerk|4|
|Monographie|1|IST_TEIL_VON|Gesamtwerk|4|

| Dateiname | Spaltenüberschriften | Beispielzeilen |
| ------ | ------ | ------ | 
|deskriptoren.csv|blattpk,pk|1,55371|
|ejahr.csv|pk,ejahr,endjahr|2309800,10,10|
|index_gesamttitel.csv|pk,name,namelower,namenorm|2200680,700 Jahre Elisabethkirche in Marburg 1283 - 1983,700 jahre elisabethkirche in marburg 1283 - 1983,700 jahre elisabethkirche in marburg 1283 - 1983|
|index_reihe.csv|pk,name,namelower,namenorm|2242470,1000 Jahre Europäische Geschichte,1000 jahre europäische geschichte,1000 jahre europaeische geschichte|
|index_sammelwerk.csv|pk,name,namelower,namenorm|1466605,10000 ans d'histoire. Dix ans de fouilles archéologiques en Alsace,10000 ans d'histoire. dix ans de fouilles archéologiques en alsace,10000 ans d'histoire. dix ans de fouilles archeologiques en alsace|
|index_zeitschrift.csv|pk,name,namelower,namenorm|42919,Der deutsche Roland,der deutsche roland,der deutsche roland|
|kurztitel.csv|pk,kurztitelnorm,kurztitel,verfasser,titel|490237,"scheffer-boichorst,_testamente_friedrichs_ii.","Scheffer-Boichorst, Testamente Friedrichs II.","Scheffer-Boichorst, Paul",Über Testamente Friedrichs II.|
|links_gesamttitel.csv|parentpk,pk,sortkey1,sortkey2,sortkey3,sortkey4|1370654,7609,4,0,0,1898|
|links_reihe.csv|parentpk,pk,sortkey1,sortkey2,sortkey3,sortkey4|2233652,552,3,0,0,1971|
|links_sammelwerk.csv|parentpk,pk,sortkey1,sortkey2,sortkey3,sortkey4|100040,7309,0,0,0,57|
|links_zeitschrift.csv|parentpk,pk,sortkey1,sortkey2,sortkey3,sortkey4|4924,9669,0,0,0,1|
|register_kurztitel.csv|pk,name,namenorm,l1,l2,counter|36904,1050-1750. Die österreichische Literatur,1050-1750. die oesterreichische literatur,49,48,1|
|register_personen.csv|pk,name,namenorm,l1,l2,counter,nachname,vorname|387085,1200 Jahre Rödelheim e.V.,1200 jahre roedelheim e.v.,49,50,1,1200 Jahre Rödelheim e.V.,""|
|register_personen_links.csv|personpk,pk|1,9644|
|systhes2.csv|pk,schlagwort,parent,bf|0,ROOT,-1,0 - 1,Geographische Zuordnung,0,0 - 2,Europa,1,0 - 3,Mitteleuropa,2,0|
|werke.csv|pk, objektart, objektartsort, auflage, beteiligtekoerperschaften, beteiligtepersonen, bibliographischeaufnahme, deskriptoren, druckort, ejahr, enthaltenebeitraege, ergaenzendeangaben, erstmals, etext, gesamttitel, herausgeber, isbn, jahrergaenzung, kurztitel, namederreihe, namederzeitschrift, namedesgesamttitels, ndjahr, ndort, originaltitel, registerindices, reihe, reihenherausgeber, sammelwerktitel, seiten, sigle, sortiertitel, sprache, zeitschriftenstatus, teilband, titel, verfasser, verlauf, volumen, zeitschrift, folgeserie, band, heftteil, jahrgang, seite, zssigle, hochschulschrift, sammlung, erstellungsdatum, aenderungsdatum, sort_ejahr, sort_seite|1,6,5,"","","","",Ungarn; Zeitschriften,Veszprém,1964 -,"","","",http://library.hungaricana.hu/en/collection/muze_szak_bako/#,"","","","","","",A Bakony termeszettudomanyi kutatasanak eredmenyei,"","","","","","","","","","","",Ungarisch,"","",A Bakony termeszettudomanyi kutatasanak eredmenyei,"",1.1964 -,"","","","","","","","","",EuropaeischeGeschichte!AMAD!,1999-10-04,2021-04-07,1964,0|
