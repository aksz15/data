// erstmal alles löschen
CALL apoc.periodic.iterate('MATCH (n) RETURN n', 'DETACH DELETE n', {batchSize:1000});
CALL apoc.schema.assert({},{},true) YIELD label, key
RETURN *;

CREATE INDEX IF NOT EXISTS FOR (n:Werk) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Werk) ON (n.objektart);
CREATE INDEX IF NOT EXISTS FOR (n:Monographie) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Sachtitelwerk) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Sammelwerk) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Gesamtwerk) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Aufsatz) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Buchbeitrag) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Reihe) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Zeitschrift) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Person) ON (n.name);
CREATE INDEX IF NOT EXISTS FOR (n:Schlagwort) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Schlagwort) ON (n.schlagwort);
CREATE INDEX IF NOT EXISTS FOR (n:Ort) ON (n.pk);
CREATE INDEX IF NOT EXISTS FOR (n:Ort) ON (n.name);
CREATE INDEX IF NOT EXISTS FOR (n:Werk) ON (n.enthaltenebeitraege);
CREATE INDEX IF NOT EXISTS FOR (n:Werk) ON (n.sammelwerktitel);
CREATE INDEX IF NOT EXISTS FOR (n:Werk) ON (n.titel);
CREATE INDEX IF NOT EXISTS FOR (n:Werk) ON (n.url);
CREATE INDEX IF NOT EXISTS FOR (n:Werk) ON (n.druckort);

// Werke importieren
:auto
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/werke.csv" AS line 
WITH line
CALL {
    WITH line
    CREATE (w:Werk)
	SET w = line}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Property sprache in Liste umwandeln
:auto
MATCH (w:Werk)
WHERE w.sprache IS NOT NULL 
WITH w
CALL {
WITH w
SET w.sprache = [x in split(w.sprache, ";") | trim(x)]
}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Property druckort in Liste umwandeln
:auto
MATCH (w:Werk)
WHERE w.sprache IS NOT NULL 
WITH w
CALL {
WITH w
SET w.druckort = [x in split(w.druckort, ";") | trim(x)]
}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Property deskriptoren in Liste umwandeln
:auto
MATCH (w:Werk)
WHERE w.deskriptoren IS NOT NULL 
WITH w
CALL {
WITH w
SET w.deskriptoren = [x in split(w.deskriptoren, ";") | trim(x)]
}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Verfasserwerk/Buch (1)
:auto
MATCH (w:Werk) WHERE w.objektart = '1'
WITH w
CALL {
    WITH w
    SET w:Monographie
    SET w.typ = 'Monographie'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Sachtitelwerk/Buchbeitrag (2)
:auto
MATCH (w:Werk) WHERE w.objektart = '2'
WITH w
CALL {
    WITH w
    SET w:Sachtitelwerk
    SET w.typ = 'Sachtitelwerk'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Sammelwerk/Buch (3)
:auto
MATCH (w:Werk) WHERE w.objektart = '3'
WITH w
CALL {
    WITH w
    SET w:Sammelwerk
    SET w.typ = 'Sammelwerk'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Gesamttitelwerk (4)
:auto
MATCH (w:Werk) WHERE w.objektart = '4'
WITH w
CALL {
    WITH w
    SET w:Gesamtwerk
    SET w.typ = 'Gesamtwerk'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Reihe (5)
:auto
MATCH (w:Werk) WHERE w.objektart = '5'
WITH w
CALL {
    WITH w
    SET w:Reihe
    SET w.typ = 'Reihe'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Zeitschrift (6)
:auto
MATCH (w:Werk) WHERE w.objektart = '6'
WITH w
CALL {
    WITH w
    SET w:Zeitschrift
    SET w.typ = 'Zeitschrift'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Buchbeitrag (7)
:auto
MATCH (w:Werk) WHERE w.objektart = '7'
WITH w
CALL {
    WITH w
    SET w:Buchbeitrag
    SET w.typ = 'Buchbeitrag'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Für Zeitschriftenaufsatz (8)
:auto
MATCH (w:Werk) WHERE w.objektart = '8'
WITH w
CALL {
    WITH w
    SET w:Aufsatz
    SET w.typ = 'Aufsatz'}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Url der Werke ergänzen
:auto
match (w:Werk)
WITH w
CALL {
    WITH w
    set w.url = 'http://opac.regesta-imperii.de/id/' + w.pk}
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Verfasser importieren
:auto
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/register_personen.csv"
AS line
WITH line
CALL {
WITH line 
CREATE (p:Person)
	SET p = line
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Thesaurus importieren
:auto
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/systhes2.csv"
AS line
WITH line
CALL {
WITH line 
CREATE (d:Schlagwort)
	SET d = line
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Thesaurushierarchie erstellen
:auto 
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/systhes2.csv"
AS line
WITH line
CALL {
WITH line 
MATCH (d1:Schlagwort {pk:line.pk}), (d2:Schlagwort {pk:line.parent})
CREATE (d1)-[:HAT_OBERBEGRIFF]->(d2)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Verfasser mit Werken verlinken
:auto
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/register_personen_links.csv"
AS line
WITH line
CALL {
WITH line 
MATCH (p:Person {pk:line.personpk}), (w:Werk {pk:line.pk})
CREATE (w)<-[:IST_VERFASSER_VON]-(p)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Buchbeitrag mit Sammelwerk verbinden
:auto 
MATCH (w1:Buchbeitrag), (w2:Sammelwerk)
WHERE w2.enthaltenebeitraege = w1.sammelwerktitel
WITH w1, w2
CALL {
    WITH w1, w2
    CREATE (w1)-[:IST_TEIL_VON {typ:"Sammelwerktitel"}]->(w2)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Gesamttitelwerk und Sammelwerk verlinken
:auto 
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/links_gesamttitel.csv"
AS line
WITH line
CALL {
WITH line 
MATCH (w1:Werk {pk:line.parentpk}), (w2:Werk {pk:line.pk})
SET w1.type = 'Gesamttitelwerk'
CREATE (w2)-[:IST_TEIL_VON {typ:"Gesamttitelwerk"}]->(w1)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Reihe verlinken
:auto 
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/links_reihe.csv"
AS line
WITH line
CALL {
WITH line 
MATCH (w1:Werk {pk:line.parentpk}), (w2:Werk {pk:line.pk})
SET w1.type = 'Reihe'
CREATE (w2)-[:IST_TEIL_VON {typ:"Reihe"}]->(w1)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Zeitschrift verlinken
:auto 
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/links_zeitschrift.csv"
AS line
WITH line
CALL {
WITH line 
MATCH (w1:Werk {pk:line.parentpk}), (w2:Werk {pk:line.pk})
SET w1.type = 'Zeitschrift'
CREATE (w2)-[:IST_TEIL_VON {typ:"Zeitschrift"}]->(w1)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Druckorte der Werke erstellen
:auto
MATCH (w:Werk)
WHERE (w.druckort) IS NOT NULL
OR (w.druckort) <> ''
WITH w.druckort as ort, w
CALL {
WITH ort, w
UNWIND ort AS ortsname
MERGE (o:Ort {name:ortsname})
MERGE (w)-[:HAT_DRUCKORT]->(o)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);
:auto
MATCH (o:Ort)
WHERE o.name STARTS WITH '['
AND o.name ENDS WITH ']'
WITH o
CALL {
WITH o
SET o.name = SUBSTRING(o.name, 1, (SIZE(o.name) - 2))
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Sprache als Knoten erstellen
:auto
MATCH (w:Werk)
WHERE (w.sprache) IS NOT NULL
OR (w.sprache) <> ''
WITH w.sprache as sprache, w
CALL {
WITH sprache, w
UNWIND sprache AS s
MERGE (o:Sprache {name:s})
MERGE (w)-[:HAT_SPRACHE]->(o)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Thesaurus mit Werken verlinken
:auto
LOAD CSV WITH HEADERS FROM "https://gitlab.rlp.net/Andreas.Kuczera/riopac2neo4j/-/raw/main/data/deskriptoren.csv"
AS line WITH line
CALL {
WITH line 
MATCH (d:Schlagwort {pk:line.blattpk}), (w:Werk {pk:line.pk})
MERGE (w)-[:HAT_SCHLAGWORT]->(d)
} IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// Herausgeberschaften ergänzen
MATCH (r:Zeitschrift)<-[rel:IST_VERFASSER_VON]-(p:Person)
CREATE (r)<-[:IST_HERAUSGEBER_VON]-(p)
DELETE rel;
MATCH (r:Reihe)<-[rel:IST_VERFASSER_VON]-(p:Person)
CREATE (r)<-[:IST_HERAUSGEBER_VON]-(p)
DELETE rel;
MATCH (r:Sammelwerk {objektart:'3'})<-[rel:IST_VERFASSER_VON]-(p:Person)
CREATE (r)<-[:IST_HERAUSGEBER_VON]-(p)
DELETE rel;
MATCH (r:Sachtitelwerk {objektart:'2'})<-[rel:IST_VERFASSER_VON]-(p:Person)
CREATE (r)<-[:IST_BEARBEITER_VON]-(p)
DELETE rel;
MATCH (r:Gesamttitelwerk {objektart:'4'})<-[rel:IST_VERFASSER_VON]-(p:Person)
CREATE (r)<-[:IST_HERAUSGEBER_VON]-(p)
DELETE rel;

// Schlagwörter kategorisieren
match p=(sr:Schlagwort {schlagwort:'Thematische Zuordnung'})<-[r:HAT_OBERBEGRIFF*..1000]-(s:Schlagwort)
where not (s:Schlagwort)<-[:HAT_OBERBEGRIFF]-()
unwind nodes(p) as n
set n.typ = 'thematisch'
return count(*);
match p=(sr:Schlagwort {schlagwort:'Geographische Zuordnung'})<-[r:HAT_OBERBEGRIFF*..1000]-(s:Schlagwort)
where not (s:Schlagwort)<-[:HAT_OBERBEGRIFF]-()
unwind nodes(p) as n
set n.typ = 'geographisch'
return count(*);
match p=(sr:Schlagwort {schlagwort:'Zeitliche Zuordnung'})<-[r:HAT_OBERBEGRIFF*..1000]-(s:Schlagwort)
where not (s:Schlagwort)<-[:HAT_OBERBEGRIFF]-()
unwind nodes(p) as n
set n.typ = 'zeitlich'
return count(*);
match p=(sr:Schlagwort {schlagwort:'Autoren/A-Z'})<-[r:HAT_OBERBEGRIFF*..1000]-(s:Schlagwort)
where not (s:Schlagwort)<-[:HAT_OBERBEGRIFF]-()
unwind nodes(p) as n
set n.typ = 'person'
return count(*);

// Pfadlänge der Schlagwörter ergänzen
MATCH p=(n1:Schlagwort)<-[r:HAT_OBERBEGRIFF*..10000]-(n2)
WHERE n1.schlagwort in ['Thematische Zuordnung','Geographische Zuordnung','Zeitliche Zuordnung']
SET n2.pfadlaenge = toInteger(length(p));

// Fixing wrong input names of PND, such as PDN or PNMD, using Levenshtein distance
MATCH (s:Schlagwort{typ: "person"})
  WHERE NOT (s)<-[:HAT_OBERBEGRIFF]-() AND NOT s.schlagwort CONTAINS "PND" AND s.schlagwort CONTAINS "="
WITH split(s.schlagwort, "=") as splitted, s
WITH apoc.text.levenshteinDistance(splitted[0], "PND") AS levenshtein, s, splitted
  WHERE levenshtein < 5
SET s.schlagwort = "PND="+splitted[1];

// Fixing when PND"number" was written
MATCH (s:Schlagwort {typ: "person"})
  WHERE s.schlagwort CONTAINS "PND" AND NOT s.schlagwort CONTAINS "="
WITH split(s.schlagwort, "D")[1] AS pnd, s
SET s.schlagwort = "PND="+pnd;

// Fixing when PND= "number"
MATCH (s:Schlagwort {typ: "person"})
  WHERE NOT (s)<-[:HAT_OBERBEGRIFF]-() AND s.schlagwort CONTAINS "PND"
  AND size(apoc.text.regexGroups(s.schlagwort, "PND=[0-9]+")) = 0
WITH trim(split(s.schlagwort, "=")[1]) as pnd_number, s
SET s.schlagwort = "PND="+pnd_number;

// Fixing PND=-"number"
MATCH (s:Schlagwort{typ:"person"})
  WHERE s.schlagwort CONTAINS "PND=" AND s.schlagwort CONTAINS "-"
WITH trim(split(s.schlagwort,"-")[1]) as number, s
SET s.schlagwort = "PND="+number;

// Adding PNDs for the ones which were PND= without anything or removing them
MATCH (s:Schlagwort{pk:"33117"})
SET s.schlagwort= "PND=137038836";

MATCH (s:Schlagwort{pk:"34195"})
SET s.schlagwort= "PND=1153499959";

MATCH (s:Schlagwort{pk:"36042"})
SET s.schlagwort= "PND=1027167187";

MATCH (s:Schlagwort{pk:"48088"})-[reli]-(w)
DELETE reli
DELETE s;

MATCH (s:Schlagwort{pk:"49727"})
SET s.schlagwort= "PND=139362983";

MATCH (s:Schlagwort{pk:"55391"})
SET s.schlagwort= "PND=134300483";

MATCH (s:Schlagwort{pk:"57593"})
SET s.schlagwort= "PND=1053241690";

MATCH (s:Schlagwort{pk:"59483"})
SET s.schlagwort= "PND=115531211";

// Add property PND to the person in Thesaurus it belongs to
MATCH (s:Schlagwort{typ: "person"}) <-[reli:HAT_OBERBEGRIFF]-(p:Schlagwort{typ: "person"})
  WHERE p.schlagwort CONTAINS "PND="
WITH split(p.schlagwort, "=")[1] as pnd_number, s
SET s += {pnd: pnd_number};

MATCH (s:Schlagwort{typ: "person"})
WHERE s.schlagwort CONTAINS "PND="
DETACH DELETE s;

MATCH (s:Schlagwort{typ:"person"})
  WHERE NOT s.schlagwort CONTAINS "Autor" AND s.schlagwort CONTAINS "("
WITH s
  WHERE size(split(s.schlagwort, "(")) = 3
WITH split(s.schlagwort, "(") as split, s
SET s.taetigkeit = "("+trim(split[1])
SET s.lebensdaten = "("+trim(split[2])
SET s.schlagwort = trim(split[0]);

MATCH (s:Schlagwort{typ:"person"})
  WHERE NOT s.schlagwort CONTAINS "Autor" AND s.schlagwort CONTAINS "("
WITH s
  WHERE size(split(s.schlagwort, "(")) = 2
WITH split(s.schlagwort, "(") as split, s
SET s.lebensdaten = "("+trim(split[1])
SET s.schlagwort = trim(split[0]);

// Verknüpfung zwischen letztem Schlagwort und Werk
:auto
MATCH (w:Werk)
CALL {
   WITH w
   match (w)-[:HAT_SCHLAGWORT]->(sw:Schlagwort), p=(sw)-[:HAT_OBERBEGRIFF*..]->(root:Schlagwort)
   where not (root)-[:HAT_OBERBEGRIFF]->()
   with w, collect(nodes(p)) as nods
   unwind nods as n
   with w, n
   where size([y in nods where all(nn in n where nn in y) ])=1
   with w, n, n[0] as dest
   MERGE (w)-[:HAT_SPEZIFISCHES_SCHLAGWORT]->(dest)
} IN TRANSACTIONS OF 100 ROWS;

// Verknüpfung zwischen letztem Schlagwort und Werk
:auto
MATCH (w:Werk)
CALL {
   WITH w
   match (w)-[:HAT_SCHLAGWORT]->(sw:Schlagwort {typ:'thematisch'}), p=(sw)-[:HAT_OBERBEGRIFF*..]->(root:Schlagwort)
   where not (root)-[:HAT_OBERBEGRIFF]->()
   with w, collect(nodes(p)) as nods
   unwind nods as n
   with w, n
   where size([y in nods where all(nn in n where nn in y) ])=1
   with w, n, n[0] as dest
   MERGE (w)-[:HAT_THEMATISCHES_SCHLAGWORT]->(dest)
} IN TRANSACTIONS OF 100 ROWS;

// Gewichte für Schlagworte ergänzen
match (s:Schlagwort)
where s.pfadlaenge is not null
set s.gewicht = 2^s.pfadlaenge;

// Merge Thesaurus persons with Person nodes
MATCH (s:Schlagwort {typ:"person"})<-[:HAT_SCHLAGWORT]-(w:Werk)<--(p:Person)
WITH trim(split(s.schlagwort, ",")[0]) as last_name, trim(split(s.schlagwort, ",")[1]) as first_name, p, s
WITH apoc.text.levenshteinSimilarity(toLower(last_name), toLower(trim(p.nachname))) as leven_last_name, apoc.text.levenshteinSimilarity(toLower(first_name), toLower(trim(p.vorname))) as leven_first_name, p, s, apoc.text.levenshteinDistance(toLower(trim(s.schlagwort)), toLower(trim(p.name))) as complete_levin
  WHERE leven_first_name > 0.6 AND leven_last_name > 0.6 AND complete_levin < 8
WITH DISTINCT p, s, leven_last_name, leven_first_name, complete_levin
CREATE (s)-[:HAT_AEHNLICHKEIT]->(p);

MATCH (s:Schlagwort)-[:HAT_AEHNLICHKEIT]->(p:Person)
WITH s, collect(p) as persons_count
  WHERE size(persons_count) > 1
WITH apoc.coll.sortMaps(persons_count, "^counter") AS sorted, s
WITH sorted[0] AS best, apoc.coll.remove(sorted, 0) AS rest, s
UNWIND rest AS person
MATCH (p:Person{pk:person.pk})-[:IST_HERAUSGEBER_VON]-(w:Werk), (bestest:Person{pk:best.pk})
MERGE (bestest)-[:IST_HERAUSGEBER_VON]->(w)
WITH bestest, person, s
MATCH (p2:Person{pk:person.pk})-[:IST_VERFASSER_VON]-(w2:Werk)
MERGE (bestest)-[:IST_VERFASSER_VON]->(w2)
WITH bestest, person, s
MATCH (p3:Person{pk:person.pk})-[:IST_BEARBEITER_VON]-(w3:Werk)
MERGE (bestest)-[:IST_BEARBEITER_VON]->(w3)
DETACH DELETE p3;

// Set properties of matching persons
MATCH (s:Schlagwort)-[:HAT_AEHNLICHKEIT]->(p:Person)
WITH s, collect(p) as persons
  WHERE size(persons) = 1
UNWIND persons as p
SET p.name = s.schlagwort
SET p.pnd = s.pnd
SET p.nachname = trim(split(s.schlagwort, ",")[0])
SET p.vorname = trim(split(s.schlagwort, ",")[1])
SET p.taetigkeit = s.taetigkeit
SET p.lebensdaten = s.lebensdaten
DETACH DELETE s;

MATCH (s: Schlagwort {typ:"person"})-[:HAT_SCHLAGWORT]-(w:Werk)
  WHERE NOT s.schlagwort CONTAINS "Autor"
CREATE (p:Person {name:s.schlagwort, vorname: trim(split(s.schlagwort, ",")[1]),  nachname: trim(split(s.schlagwort, ",")[1]), pnd: s.pnd, lebensdaten: s.lebensdaten, taetigkeit: s.taetigkeit, namenorm: toLower(s.schlagwort)})-[:IST_VERFASSER_VON]->(w)
DETACH DELETE s;

MATCH (s:Schlagwort{typ:"person"})
DETACH DELETE s;


// Luceneindexe erstellen
// On: Werk Titel
CREATE FULLTEXT INDEX werkTitel IF NOT EXISTS FOR (w:Werk) ON EACH [w.titel];
// On: Werk Titel & Erscheinungsjahr & Person Autor
CREATE FULLTEXT INDEX werkTitelJahrAutor IF NOT EXISTS FOR (w:Werk|Person) ON EACH [w.titel, w.ejahr,w.name];
// Property mit Titel, Personen, Schlagwörtern und Orten erstellen
:auto
match (w:Werk)
where w.titel is not null 
//optional match (w)-[:HAT_DRUCKORT]->(o:Ort), (s:Schlagwort)<-[:HAT_SPEZIFISCHES_SCHLAGWORT]-(w), (w)--(p:Person)
WITH w, w.titel as stichwoerter
CALL {
    WITH w, stichwoerter
    set w.stichwoerter = stichwoerter} 
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);
:auto
match (w:Werk)<--(p:Person)
//optional match (w)-[:HAT_DRUCKORT]->(o:Ort), (s:Schlagwort)<-[:HAT_SPEZIFISCHES_SCHLAGWORT]-(w), (w)--(p:Person)
WITH w, p.name as stichwoerter
CALL {
    WITH w, stichwoerter
    set w.stichwoerter = w.stichwoerter + ' ' + stichwoerter} 
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);
:auto
match (s:Schlagwort)<-[:HAT_SPEZIFISCHES_SCHLAGWORT]-(w:Werk)
WITH w, s.schlagwort as stichwoerter
CALL {
    WITH w, stichwoerter
    set w.stichwoerter = w.stichwoerter + ' ' + stichwoerter} 
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);
:auto
match (w:Werk)-[:HAT_DRUCKORT]->(o:Ort)
WITH w, o.name as stichwoerter
CALL {
    WITH w, stichwoerter
    set w.stichwoerter = w.stichwoerter + ' ' + stichwoerter} 
IN TRANSACTIONS OF 1000 ROWS
RETURN count(*);

// On: Werk Titel, Personen, Schlagwörtern und Orte
CREATE FULLTEXT INDEX stichwoerter IF NOT EXISTS FOR (w:Werk) ON EACH [w.stichwoerter];

// Beispielabfragen:
// Call Befehl: Werk Titel
CALL db.index.fulltext.queryNodes("werkTitel", "Titel") 
YIELD node, score RETURN node.titel, score;
// Call Befehl: Werk Titel & Erscheinungsjahr
CALL db.index.fulltext.queryNodes("werkTitelJahrAutor", "19??") 
YIELD node, score RETURN node.titel, node.ejahr, score;
CALL db.index.fulltext.queryNodes("werkTitelJahrAutor", "Arnsburg") 
YIELD node, score RETURN node.titel, score;
// 
//

// Query um ähnliche personen in den Personen Knoten zu finden

// Filter
// MATCH (s:Schlagwort {typ:"person"})<-[:HAT_SCHLAGWORT]-(w:Werk)<--(p:Person)
// WITH trim(split(s.schlagwort, ",")[0]) as last_name, trim(split(s.schlagwort, ",")[1]) as first_name, p, s
// WITH apoc.text.levenshteinSimilarity(toLower(last_name), toLower(trim(p.nachname))) as leven_last_name, apoc.text.levenshteinSimilarity(toLower(first_name), toLower(trim(p.vorname))) as leven_first_name, p, s, apoc.text.levenshteinDistance(toLower(trim(s.schlagwort)), toLower(trim(p.name))) as complete_levin
// WHERE leven_first_name > 0.6 AND leven_last_name > 0.6 AND complete_levin < 8
// WITH DISTINCT p, s, leven_last_name, leven_first_name, complete_levin
// RETURN leven_last_name, leven_first_name, complete_levin, p.name, s.schlagwort;

// Filter opposite
// MATCH (s:Schlagwort {typ:"person"})<-[:HAT_SCHLAGWORT]-(w:Werk)<--(p:Person)
// WITH trim(split(s.schlagwort, ",")[0]) as last_name, trim(split(s.schlagwort, ",")[1]) as first_name, p, s
// WITH apoc.text.levenshteinSimilarity(toLower(last_name), toLower(trim(p.nachname))) as leven_last_name, apoc.text.levenshteinSimilarity(toLower(first_name), toLower(trim(p.vorname))) as leven_first_name, p, s, apoc.text.levenshteinDistance(toLower(trim(s.schlagwort)), toLower(trim(p.name))) as complete_levin
// WHERE leven_first_name > 0.6 AND leven_last_name > 0.6 AND complete_levin < 8
// WITH DISTINCT p, s AS macthedSchlagwort , leven_last_name, leven_first_name, complete_levin
// WITH collect(macthedSchlagwort.pk) AS matchedSchlagwortList
// MATCH (s:Schlagwort{typ:"person"})
// WHERE NOT apoc.coll.contains(matchedSchlagwortList, s.pk)
// RETURN s.pk, s.schlagwort;



