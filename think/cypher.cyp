match (n) detach delete n;

//https://git.thm.de/aksz15/data/-/raw/master/think/plaintext.txt
LOAD CSV WITH HEADERS FROM "https://git.thm.de/aksz15/data/-/raw/master/think/cleaned_text.txt" AS text
WITH text
CREATE (t:Text)
SET t.text = text.text;

:auto MATCH (r:Text)
WHERE r.text is not null AND NOT((r)-[:NEXT_TOKEN]->())
CALL {
WITH r
CALL thm.fullChain(r, "text", true)
} IN TRANSACTIONS OF 1 ROWS
RETURN count(*);

LOAD CSV WITH HEADERS FROM "https://git.thm.de/aksz15/data/-/raw/master/think/annotations.csv" AS line
match (c1:Character {startIndex:toInteger(line.start)})
match (c2:Character {startIndex:toInteger(line.end)})
create (s:Spo {type:line.type, subtype:line.subtype})
CREATE (s)-[:FIRST_CHARACTER]->(c1)
CREATE (s)-[:LAST_CHARACTER]->(c2);

